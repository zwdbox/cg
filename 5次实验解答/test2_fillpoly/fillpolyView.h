// fillpolyView.h : interface of the CFillpolyView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_FILLPOLYVIEW_H__C9F39126_0F35_4A9E_9FAA_74C3306D4994__INCLUDED_)
#define AFX_FILLPOLYVIEW_H__C9F39126_0F35_4A9E_9FAA_74C3306D4994__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "base.h"
 
class CFillpolyView : public CView
{
protected: // create from serialization only
	CFillpolyView();
	DECLARE_DYNCREATE(CFillpolyView)

	POLYGON poly;
	POINT points[20000];//点列
	int pntnum[200];//多边形数量数组
	int plynum;//多边形数量
	int pointcount;//点的总数量
// Attributes
public:
	CFillpolyDoc* GetDocument();
	bool ClosePoly();
	void Clear();
// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFillpolyView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CFillpolyView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CFillpolyView)
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in fillpolyView.cpp
inline CFillpolyDoc* CFillpolyView::GetDocument()
   { return (CFillpolyDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FILLPOLYVIEW_H__C9F39126_0F35_4A9E_9FAA_74C3306D4994__INCLUDED_)
