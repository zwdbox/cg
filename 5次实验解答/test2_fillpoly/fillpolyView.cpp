// fillpolyView.cpp : implementation of the CFillpolyView class
//

#include "stdafx.h"
#include "fillpoly.h"

#include "fillpolyDoc.h"
#include "fillpolyView.h"

#include "base.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFillpolyView

IMPLEMENT_DYNCREATE(CFillpolyView, CView)

BEGIN_MESSAGE_MAP(CFillpolyView, CView)
	//{{AFX_MSG_MAP(CFillpolyView)
	ON_WM_LBUTTONDOWN()
	ON_WM_RBUTTONDOWN()
	ON_WM_CHAR()
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFillpolyView construction/destruction

CFillpolyView::CFillpolyView()
{
	// TODO: add construction code here
	Clear();
	/*
	//int pntnum[]={4,5,5};
	pntnum[0]=4;
	pntnum[1]=5;
	pntnum[2]=5;
	point[0].x=265;
	point[0].y=5;
	point[1].x=560;
	point[1].y=90;
	point[2].x=462;
	point[2].y=400;
	point[3].x=60;
	point[3].y=220;

	point[4].x=50;
	point[4].y=50;
	point[5].x=410;
	point[5].y=50;
	point[6].x=200;
	point[6].y=401;
	point[7].x=130;
	point[7].y=230;
	point[8].x=10;
	point[8].y=400;

	point[9].x=300;
	point[9].y=300;
	point[10].x=300;
	point[10].y=100;
	point[11].x=450;
	point[11].y=310;
	point[12].x=450;
	point[12].y=100;
	point[13].x=300;
	point[13].y=350;
	poly.plynum=3;
	*/
}

CFillpolyView::~CFillpolyView()
{
}

BOOL CFillpolyView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CFillpolyView drawing

void CFillpolyView::OnDraw(CDC* pDC)
{
	CFillpolyDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	// TODO: add draw code for native data here
	//CPaintDC dc(this); // device context for painting
	CClientDC dc(this);
	CPen Pen,*OldPen;
	Pen.CreatePen(PS_SOLID,1,RGB(255,0,0));
	OldPen=dc.SelectObject(&Pen);
	CString str,str1;
	
	dc.TextOut(10,10,"鼠标左键绘制边，鼠标右键关闭边，键盘s填充多边形，c清除重新绘制。");
	str.Format("点总数量=%d,边数量=%d:[",pointcount,plynum);
	for(int i=0;i<=plynum;i++)
	{
		str1.Format("%d",pntnum[i]);
		if(i>0)
			str+=","+str1;
		else
			str+=str1;
	}
	str+="]";
	
	dc.TextOut(10,30,str);
	//绘制直线
	int m,n;
	int idx=0;
	for(m=0;m<=plynum;m++)
	/*循环多边形每个每个边*/
	{
		if (pntnum[m]==0)//如果边的点数量为0，中断循环
			break;
		dc.MoveTo(points[idx].x,points[idx].y);
		idx++;
		for(n=1;n<pntnum[m];n++)
		{			
			dc.LineTo(points[idx].x,points[idx].y);	
			idx++;
			if(m<plynum && n==pntnum[m]-1) 
			{//如果不是最后一个边，关闭边
				dc.LineTo(points[idx-pntnum[m]].x,points[idx-pntnum[m]].y);	
			}
		}
	}
	CPen Pen2;
	Pen2.CreatePen(PS_SOLID,1,RGB(0,255,0));
	dc.SelectObject(&Pen2);
	if (poly.plynum>0)
		DrawPolygon(dc.m_hDC,&poly);
	dc.SelectObject(OldPen);
}

/////////////////////////////////////////////////////////////////////////////
// CFillpolyView printing

BOOL CFillpolyView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CFillpolyView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CFillpolyView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

/////////////////////////////////////////////////////////////////////////////
// CFillpolyView diagnostics

#ifdef _DEBUG
void CFillpolyView::AssertValid() const
{
	CView::AssertValid();
}

void CFillpolyView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CFillpolyDoc* CFillpolyView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CFillpolyDoc)));
	return (CFillpolyDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CFillpolyView message handlers

void CFillpolyView::OnLButtonDown(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	//this.point[pointcount++].x=point.x;
	points[pointcount].x=point.x;
	points[pointcount].y=point.y;	
	pntnum[plynum]++;
	pointcount++;
	Invalidate(true);
	CView::OnLButtonDown(nFlags, point);
}

void CFillpolyView::Clear()
{
	poly.point=points;
	poly.pntnum=pntnum;
	poly.plynum=0;
	plynum=0;
	pointcount=0;
	for(int i=0;i<200;i++)
	{
		pntnum[i]=0;
	}
}
//闭合当前多边形
bool CFillpolyView::ClosePoly()
{
	if(pntnum[plynum]>=3)
	{
		plynum++;		
		return true;
	}else if(pntnum[plynum]>0)
	{
		MessageBox("需要至少3个点才能闭合多边形","提示", MB_ICONEXCLAMATION );
		return false;
	}
	return true;
}
void CFillpolyView::OnRButtonDown(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	//闭合当前多边形
	if (ClosePoly())
		Invalidate(true);
	CView::OnRButtonDown(nFlags, point);
}

void CFillpolyView::OnChar(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	// TODO: Add your message handler code here and/or call default
	if(nChar=='s'||nChar=='S')
	{
		if (ClosePoly())
		{
			poly.plynum=plynum;
			Invalidate(true);
		}
	} else
	if(nChar=='c'||nChar=='C')
	{
		Clear();
		Invalidate(true);
	}
	else
	{
		MessageBox("错误的按键。","提示", MB_ICONEXCLAMATION );
	}
	CView::OnChar(nChar, nRepCnt, nFlags);
}
