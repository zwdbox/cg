// fillpolyDoc.cpp : implementation of the CFillpolyDoc class
//

#include "stdafx.h"
#include "fillpoly.h"

#include "fillpolyDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFillpolyDoc

IMPLEMENT_DYNCREATE(CFillpolyDoc, CDocument)

BEGIN_MESSAGE_MAP(CFillpolyDoc, CDocument)
	//{{AFX_MSG_MAP(CFillpolyDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFillpolyDoc construction/destruction

CFillpolyDoc::CFillpolyDoc()
{
	// TODO: add one-time construction code here

}

CFillpolyDoc::~CFillpolyDoc()
{
}

BOOL CFillpolyDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CFillpolyDoc serialization

void CFillpolyDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CFillpolyDoc diagnostics

#ifdef _DEBUG
void CFillpolyDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CFillpolyDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CFillpolyDoc commands
