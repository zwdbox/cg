// TestView.h : interface of the CTestView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_TESTVIEW_H__A75FDCFB_621C_4E38_A154_C344803E6372__INCLUDED_)
#define AFX_TESTVIEW_H__A75FDCFB_621C_4E38_A154_C344803E6372__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include "Line.h"//包含直线类
#include "Transform.h"

#define POINT_NUM 5
#define PICK_WIDTH 6

class CTestView : public CView
{
protected: // create from serialization only
	CTestView();
	DECLARE_DYNCREATE(CTestView)

// Attributes
public:
	CTestDoc* GetDocument();
	CString debugStr;
// Operations
public:
	void DoubleBuffer();//双缓冲
	void ReadPoint();//读入顶点表
	void DrawObject(CDC *);//绘制图形
	void DrawPolygon(CDC *pDC);//绘制多边形
	void CalcRoundRect();//计算包围矩形
	void RestoreData();
	void BackData();
	CRgn* CreateRgn();
	void GetMousePick(int mouse_x,int mouse_y);
	CPoint DPtoLP(CPoint point);
	void CalcRotatePoint();
	double ScaleBaseSide(CP2 P1,CP2 P2,CP2 P_Old,POINT P_New,bool direction_x,double ref_scale);
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTestView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnInitialUpdate();
	protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CTestView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:	
	CP2 P[POINT_NUM];//点表
	CP2 CtrlPoint[10];//控制点表:0:左上，1:上中，2:右上，3:右中，4:右下，5:下中，6:左下，7左中，8:旋转点，9:中心点

	CP2 P_BK[POINT_NUM];//点表的备份
	CP2 CtrlPoint_BK[10];//控制点表的备份

	int MousePick;//鼠标击中的控制点：-1:没有控制点。0-9对应CtrlPoint[]的下标
	bool SHIFT_DOWN;
	CRect rect;//定义客户区
	CTransform trans;
	CPoint MouseOldPos;
	bool dragging;
	// Generated message map functions
protected:
	//{{AFX_MSG(CTestView)
	afx_msg void OnDrawpic();
	afx_msg void OnLeft();
	afx_msg void OnRight();
	afx_msg void OnUp();
	afx_msg void OnDown();
	afx_msg void OnIncrease();
	afx_msg void OnDecrease();
	afx_msg void OnCounterClockwise();
	afx_msg void OnClockwise();
	afx_msg void OnXaxis();
	afx_msg void OnYaxis();
	afx_msg void OnOrg();
	afx_msg void OnYDirectionPlus();
	afx_msg void OnXDirectionNeg();
	afx_msg void OnXDirectionPlus();
	afx_msg void OnYDirectionNeg();
	afx_msg void OnReset();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in TestView.cpp
inline CTestDoc* CTestView::GetDocument()
   { return (CTestDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TESTVIEW_H__A75FDCFB_621C_4E38_A154_C344803E6372__INCLUDED_)
