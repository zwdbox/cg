// TestView.cpp : implementation of the CTestView class
//

#include "stdafx.h"
#include "Test.h"
#include "TestDoc.h"
#include "TestView.h"
#include "math.h"
#define Round(d) int(floor(d+0.5))//四舍五入宏定义
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// CTestView

IMPLEMENT_DYNCREATE(CTestView, CView)

BEGIN_MESSAGE_MAP(CTestView, CView)
	//{{AFX_MSG_MAP(CTestView)
	ON_COMMAND(IDM_DRAWPIC, OnDrawpic)
	ON_COMMAND(ID_TLEFT, OnLeft)
	ON_COMMAND(ID_TRIGHT, OnRight)
	ON_COMMAND(ID_TUP, OnUp)
	ON_COMMAND(ID_TDOWN, OnDown)
	ON_COMMAND(ID_SINCREASE, OnIncrease)
	ON_COMMAND(ID_SDECREASE, OnDecrease)
	ON_COMMAND(ID_RCLOCKWISE, OnCounterClockwise)
	ON_COMMAND(ID_RCOUNTERCLOCKWISE, OnClockwise)
	ON_COMMAND(ID_RXAXIS, OnXaxis)
	ON_COMMAND(ID_RYAXIS, OnYaxis)
	ON_COMMAND(ID_RORG, OnOrg)
	ON_COMMAND(ID_SYDIRECTIONPLUS, OnYDirectionPlus)
	ON_COMMAND(ID_SYDIRECTIONNEG, OnXDirectionNeg)
	ON_COMMAND(ID_SXDIRECTIONPLUS, OnXDirectionPlus)
	ON_COMMAND(ID_SXDIRECTIONNEG, OnYDirectionNeg)
	ON_COMMAND(ID_RESET, OnReset)
	ON_WM_MOUSEMOVE()
	ON_WM_SETCURSOR()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_KEYDOWN()
	ON_WM_KEYUP()
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTestView construction/destruction

CTestView::CTestView()
{
	dragging=false;
	SHIFT_DOWN=false;
	// TODO: add construction code here
}

CTestView::~CTestView()
{
}

BOOL CTestView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CTestView drawing

void CTestView::OnDraw(CDC* pDC)
{
	CTestDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	// TODO: add draw code for native data here	
	DoubleBuffer();
	pDC->TextOut(10,10,debugStr);
}

/////////////////////////////////////////////////////////////////////////////
// CTestView printing

BOOL CTestView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CTestView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CTestView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

/////////////////////////////////////////////////////////////////////////////
// CTestView diagnostics

#ifdef _DEBUG
void CTestView::AssertValid() const
{
	CView::AssertValid();
}

void CTestView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CTestDoc* CTestView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CTestDoc)));
	return (CTestDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CTestView message handlers
void CTestView::DoubleBuffer()//双缓冲
{
	CDC* pDC=GetDC();
	GetClientRect(&rect);//获得客户区的大小
	pDC->SetMapMode(MM_ANISOTROPIC);//pDC自定义坐标系
	pDC->SetWindowExt(rect.Width(),rect.Height());//设置窗口范围
	pDC->SetViewportExt(rect.Width(),-rect.Height());//x轴水平向右，y轴垂直向上
	pDC->SetViewportOrg(rect.Width()/2,rect.Height()/2);//屏幕中心为原点
	CDC MemDC;//内存DC
	CBitmap NewBitmap,*pOldBitmap;//内存中承载图像的临时位图
	MemDC.CreateCompatibleDC(pDC);//建立与屏幕pDC兼容的MemDC 
	NewBitmap.CreateCompatibleBitmap(pDC,rect.Width(),rect.Height());//创建兼容位图 
	pOldBitmap=MemDC.SelectObject(&NewBitmap); //将兼容位图选入MemDC 
	MemDC.FillSolidRect(&rect,pDC->GetBkColor());//按原来背景填充客户区，否则是黑色
	MemDC.SetMapMode(MM_ANISOTROPIC);//MemDC自定义坐标系
	MemDC.SetWindowExt(rect.Width(),rect.Height());
	MemDC.SetViewportExt(rect.Width(),-rect.Height());
	MemDC.SetViewportOrg(rect.Width()/2,rect.Height()/2);
	DrawObject(&MemDC);
	pDC->BitBlt(-rect.Width()/2,-rect.Height()/2,
		rect.Width(),rect.Height(),
		&MemDC,-rect.Width()/2,
		-rect.Height()/2,SRCCOPY);//将内存位图拷贝到屏幕
	MemDC.SelectObject(pOldBitmap);//恢复位图
	NewBitmap.DeleteObject();//删除位图
	ReleaseDC(pDC);//释放DC	
}

//point是设备坐标（通常是鼠标），返回逻辑坐标
CPoint CTestView::DPtoLP(CPoint point)
{
	CDC* pDC=GetDC();
	POINT pnt;
	pnt.x=point.x;
	pnt.y=point.y;
	pDC->SetMapMode(MM_ANISOTROPIC);//pDC自定义坐标系
	pDC->SetWindowExt(rect.Width(),rect.Height());//设置窗口范围
	pDC->SetViewportExt(rect.Width(),-rect.Height());//x轴水平向右，y轴垂直向上
	pDC->SetViewportOrg(rect.Width()/2,rect.Height()/2);//屏幕中心为原点
	pDC->DPtoLP(&pnt,1);
	return pnt;
}

//逻辑坐标
//返回鼠标命中点。
//返回0-7：包围矩形上的控制点
//返回8：旋转点，返回9命中了多边形，用于拖动
//CP2 CtrlPoint[10];
//控制点表:0:左上，1:上中，2:右上，3:右中，4:右下，5:下中，6:左下，7左中，8:旋转点，9:中心点
void CTestView::GetMousePick(int mouse_x,int mouse_y)
{
	int i;
	MousePick=-1;
	for(i=0;i<9;i++)
	{
		if(mouse_x>=CtrlPoint[i].x-PICK_WIDTH && mouse_x<=CtrlPoint[i].x+PICK_WIDTH
			&&
		   mouse_y>=CtrlPoint[i].y-PICK_WIDTH && mouse_y<=CtrlPoint[i].y+PICK_WIDTH	
		)
		{
			MousePick=i;
			return;
		}
	}
	CRgn *rgn=CreateRgn();
	if(rgn->PtInRegion(mouse_x,mouse_y))
		MousePick=9;
	delete rgn;
}
void CTestView::ReadPoint()//点表
{
	P[0].x=-100;P[0].y=-50;
	P[1].x=100; P[1].y=-250;
	P[2].x=160; P[2].y=150;
	P[3].x=-60;P[3].y=100;
	P[4].x=-180;P[4].y=250;
	CalcRoundRect();
}

//求延长线上点的坐标
//https://zhidao.baidu.com/question/147132202.html
void CTestView::CalcRotatePoint()
{
	/*先根据A,B的坐标，求出直线AB的方程，然后设出C点坐标（x,y)，根据BC的长度得到一个方程，根据C点在直线上得到另一个方程，联立这两个方程求出x,y，即知点C的坐标。*/
	double xa,ya,xb,yb,xc,yc;
	double ab,bc=30;
	xa=CtrlPoint[9].x;
	ya=CtrlPoint[9].y;
	xb=CtrlPoint[1].x;
	yb=CtrlPoint[1].y;

	ab = sqrt( (xb-xa)* (xb-xa) + (yb-ya)* (yb-ya) );
	xc=xb+bc/ab*(xb-xa);
	yc=yb+bc/ab*(yb-ya);

	CtrlPoint[8].x=xc;
	CtrlPoint[8].y=yc;
}
void CTestView::CalcRoundRect()//计算包围矩形
{
	int m,pn;
	double x_min,y_min,x_max,y_max;
	pn=POINT_NUM;
	x_min=P[0].x;
	y_min=P[0].y;
	x_max=x_min;
	y_max=y_min;
	/*填充多边形边表*/
	for(m=1;m<pn;m++) /*P_num(j) +2 while side_num(i) +1*/
		/*每个点循环*/
	{
		if (P[m].x<x_min)
			x_min=P[m].x;
		if  (P[m].x>x_max)
			x_max=P[m].x;
		if (P[m].y<y_min)
			y_min=P[m].y;
		if  (P[m].y>y_max)
			y_max=P[m].y;
	}
	CtrlPoint[0].x=x_min;//left top
	CtrlPoint[0].y=y_max;
	CtrlPoint[1].x=(x_min+x_max)/2.0;//top center
	CtrlPoint[1].y=y_max;
	CtrlPoint[2].x=x_max;//right top
	CtrlPoint[2].y=y_max;
	CtrlPoint[3].x=x_max;//right center;
	CtrlPoint[3].y=(y_min+y_max)/2.0;
	CtrlPoint[4].x=x_max;//right bottom
	CtrlPoint[4].y=y_min;
	CtrlPoint[5].x=(x_min+x_max)/2.0;//bottom center
	CtrlPoint[5].y=y_min;
	CtrlPoint[6].x=x_min;//left bottom
	CtrlPoint[6].y=y_min;
	CtrlPoint[7].x=x_min;//left bottom
	CtrlPoint[7].y=(y_min+y_max)/2.0;

	CtrlPoint[8].x=(x_min+x_max)/2.0;//rotate point
	CtrlPoint[8].y=y_max+10;

	CtrlPoint[9].x=(x_min+x_max)/2.0;//center
	CtrlPoint[9].y=(y_min+y_max)/2.0;

	CalcRotatePoint();
}

void CTestView::DrawObject(CDC *pDC)//绘制图形
{
	CLine *line=new CLine;//绘制坐标系
	line->MoveTo(pDC,0,-rect.Height()/2);//绘制x轴
	line->LineTo(pDC,0,rect.Height()/2);
	line->MoveTo(pDC,-rect.Width()/2,0);//绘制y轴
	line->LineTo(pDC,rect.Width()/2,0);
	DrawPolygon(pDC);
	delete line;
}

CRgn* CTestView::CreateRgn()
{
	POINT pptl[POINT_NUM];//绘制多边形时使用
	int i;
	CRgn *rgn=new CRgn();
	for(i=0;i<POINT_NUM;i++)
	{
		pptl[i].x=(long)P[i].x;
		pptl[i].y=(long)P[i].y;
	}
	rgn->CreatePolygonRgn(pptl,POINT_NUM,ALTERNATE);
	return rgn;
}
void CTestView::DrawPolygon(CDC *pDC)//绘制多边形线框模型
{
	
	CLine *line=new CLine;
	CP2 t;
	CPen Pen,Pen2,*OldPen;
	Pen.CreatePen(PS_SOLID,1,RGB(255,0,0));
	Pen2.CreatePen(PS_SOLID,1,RGB(127,127,127));
	OldPen=pDC->SelectObject(&Pen);

	CBrush brush;
	brush.CreateSolidBrush(RGB(255,0,0));
	CRgn *rgn=CreateRgn();
	pDC->FillRgn(rgn,&brush);
	delete rgn;
	for(int i=0;i<POINT_NUM;i++)//绘制多边形
	{
		if(i==0)
		{
			line->MoveTo(pDC,P[i]);
			t=P[i];
		}
		else
		{
			line->LineTo(pDC,P[i]);
		}		
	}
	line->LineTo(pDC,t);//闭合多边形
	delete line;

	pDC->SelectObject(&Pen2);

	pDC->MoveTo((int)CtrlPoint[0].x,(int)CtrlPoint[0].y);
	pDC->LineTo((int)CtrlPoint[2].x,(int)CtrlPoint[2].y);
	pDC->LineTo((int)CtrlPoint[4].x,(int)CtrlPoint[4].y);
	pDC->LineTo((int)CtrlPoint[6].x,(int)CtrlPoint[6].y);
	pDC->LineTo((int)CtrlPoint[0].x,(int)CtrlPoint[0].y);

	pDC->MoveTo((int)CtrlPoint[1].x,(int)CtrlPoint[1].y);
	pDC->LineTo((int)CtrlPoint[8].x,(int)CtrlPoint[8].y);
	

	for (i=0;i<9;i++)
	{
		pDC->Ellipse((int)(CtrlPoint[i].x-PICK_WIDTH),(int)(CtrlPoint[i].y-PICK_WIDTH),
			(int)(CtrlPoint[i].x+PICK_WIDTH),(int)(CtrlPoint[i].y+PICK_WIDTH));
	}

	pDC->SelectObject(OldPen);
}

void CTestView::OnDrawpic() 
{
	// TODO: Add your command handler code here
	MessageBox("请使用图标按钮进行二维几何变换","提示",MB_OK);
}

void CTestView::OnLeft()//向左平移 
{
	// TODO: Add your command handler code here
	trans.Translate(-10,0);
	Invalidate(FALSE);
}

void CTestView::OnRight()//向右平移 
{
	// TODO: Add your command handler code here
	trans.Translate(10,0);
	DoubleBuffer();
}

void CTestView::OnUp()//向上平移  
{
	// TODO: Add your command handler code here
	trans.Translate(0,10);
	Invalidate(FALSE);	
}

void CTestView::OnDown()//向下平移 
{
	// TODO: Add your command handler code here
    trans.Translate(0,-10);
	Invalidate(FALSE);	
}

void CTestView::OnIncrease()//等比放大  
{
	// TODO: Add your command handler code here
    trans.Scale(2,2);
	Invalidate(FALSE);	
}

void CTestView::OnDecrease()//等比缩小 
{
	// TODO: Add your command handler code here
    trans.Scale(0.5,0.5);
	Invalidate(FALSE);	
}

void CTestView::OnClockwise()//顺时针复合旋转  
{
	// TODO: Add your command handler code here
	CP2 p=(P[0]+P[2])/2;
	trans.Rotate(-30,p);
	Invalidate(FALSE);
}

void CTestView::OnCounterClockwise()//逆时针复合旋转 
{
	// TODO: Add your command handler code here
	CP2 p=(P[0]+P[2])/2;
    trans.Rotate(30,p);
	Invalidate(FALSE);		
}

void CTestView::OnXaxis()//关于x轴反射 
{
	// TODO: Add your command handler code here
    trans.ReflectX();
	Invalidate(FALSE);	
}

void CTestView::OnYaxis()//关于y轴反射 
{
	// TODO: Add your command handler code here
    trans.ReflectY();
	Invalidate(FALSE);	
}

void CTestView::OnOrg()//关于原点反射 
{
	// TODO: Add your command handler code here
    trans.ReflectOrg();
	Invalidate(FALSE);
}

void CTestView::OnXDirectionPlus()//沿x轴正向错切  
{
	// TODO: Add your command handler code here
	trans.Shear(0,1);
	Invalidate(FALSE);	
}

void CTestView::OnXDirectionNeg()//沿x轴负向错切
{
	// TODO: Add your command handler code here
	trans.Shear(0,-1);
	Invalidate(FALSE);	
}

void CTestView::OnYDirectionPlus()//沿y轴正向错切 
{
	// TODO: Add your command handler code here
	trans.Shear(1,0);
	Invalidate(FALSE);	
}

void CTestView::OnYDirectionNeg()//沿y轴负向错切 
{
	// TODO: Add your command handler code here
	trans.Shear(-1,0);
	Invalidate(FALSE);	
}

void CTestView::OnReset()//复位 
{
	// TODO: Add your command handler code here
	ReadPoint();
	Invalidate(FALSE);	
}

void CTestView::OnInitialUpdate() 
{
	CView::OnInitialUpdate();
	// TODO: Add your specialized code here and/or call the base class
	ReadPoint();
	trans.SetMat(P,POINT_NUM,CtrlPoint);	
}

//返回度，比如30，表示30度
double CalcTheta(double a1,double b1,double a2,double b2)
{//https://blog.csdn.net/qq_39534332/article/details/100170970
	//由向量叉乘判断正负：
	//a X b = |a| * |b| * sin<a,b>=a.x * b.y - a.y * b.x;
	//如果aXb < 0,那么 <a,b> = -<a,b>
	double f=1;
	double rr=1;
	if ((a1==0 && b1==0) ||(a2==0 && b2==0))
		return 0.0;
	if (a1*b2-a2*b1<0)
		f=-1;
	//double r = atan2(a1*a2+b1*b2,a1*b2-a2*b1);	
	rr=(a1*a2+b1*b2)/(sqrt(a1*a1+b1*b1)*sqrt(a2*a2+b2*b2));	
	rr = acos(rr);
	return rr * 180.0/3.14159265 * f;
}

//点(x,y)与点(X,Y)是否在直线(x1,y1),(x2,y2)的同一侧
//https://blog.csdn.net/qq_27606639/article/details/80905311
bool IsSameSide(double x1,double y1,double x2,double y2,double x,double y,double X,double Y)
{
	double A,B,C;
	double a1,b1;
	A=y2-y1;
	B=x1-x2;
	C=x2*y1-x1*y2;
	a1=A*x+B*y+C;
	b1=A*X+B*Y+C;
	if(a1*b1>=0)
		return true;
	return false;
}
//点(x0,y0)到直线(x1,y1),(x2,y2)的距离 
double CalcDist(double x1,double y1,double x2,double y2,double x0,double y0)
{
	double res,a,b,c;//以双精度保存变量
	//cin>>x1>>y1>>x2>>y2>>x0>>y0;//输入三组数据
	a=(x0-x1)*(y2-y1);//分子的左半部分
	b=(y0-y1)*(x1-x2);//分子的右半部分
	c=a+b;//二者相加
	c*=c;//平方(pow(c,2)貌似在这里更加麻烦)
	a=pow(y2-y1,2);//分母左半部分
	b=pow(x1-x2,2);//分母右半部分
	c/=(a+b);//分子分母相除
	res=sqrt(c);//开方
	return res;
}

//计算基于直线（x1,y1),(x2,y2)的新旧两点离直线的距离的比例。
//返回0，表示不能计算
double CalcScale(double x1,double y1,
				  double x2,double y2,
				  double old_x,double old_y,
				  double new_x,double new_y)
{
	double sy;
	double dist_new=CalcDist(x1,y1,x2,y2,new_x,new_y);
	double dist_old=CalcDist(x1,y1,x2,y2,old_x,old_y);
	if (dist_old==0)
		return 0;
	sy=dist_new/dist_old;
	return sy;
}

//基于某一边的比例变换
//P1,P2是对边的直线段,P2也是缩放基点
//P_Old是这边的原始点（旧点）
//Point是鼠标点（新点）
//direction_x：true表示X方向缩放，false表示Y方向缩放
//ref_scale是以前的比例，0表示本次计算比例，不为0表示使用以前的比例。用于等比例缩放
double CTestView::ScaleBaseSide(CP2 P1,CP2 P2,CP2 P_Old,POINT P_New,bool direction_x,double ref_scale)
{
	double sx,sy;
	double s;
	if (ref_scale==0)
	{
		s=CalcScale(
			P1.x,P1.y,
			P2.x,P2.y,
			P_Old.x,P_Old.y,
			P_New.x,P_New.y
			);
		if(s==0)
			return 0;
	} else
	{
		if(ref_scale>0)
			s=ref_scale;
		else
			s=-ref_scale;
	}
	if(!IsSameSide(P1.x,P1.y,P2.x,P2.y,P_Old.x,P_Old.y,
		P_New.x,P_New.y))
		s*=-1;
	if(direction_x)
	{//X方向缩放
		sx=s;
		sy=1.0;
	}
	else //Y方向缩放
	{
		sx=1.0;
		sy=s;
	}
	//[9]中心点，[8]旋转点
	double theta=CalcTheta(
		CtrlPoint[8].x-CtrlPoint[9].x,CtrlPoint[8].y-CtrlPoint[9].y,
		0,100);		
	trans.Rotate(theta,P2);
	trans.Scale(sx,sy,P2);
	trans.Rotate(-theta,P2);
	return s;
}
void CTestView::OnMouseMove(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	point=DPtoLP(point);
	CP2 basePoint,P1,P2,P_Old;
	CP2 p;
	double theta;
	double s;	
	if(dragging==false)
		GetMousePick(point.x,point.y);

	if(nFlags & MK_LBUTTON)
	{		
		s=0;
		dragging=true;	
		switch(MousePick)
		{
		case 0:
		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
		case 6:
		case 7:
			if (MousePick==0 || MousePick==1 || MousePick==2)
			{
				P_Old=CtrlPoint[1];
				P1=CtrlPoint[4];
				P2=CtrlPoint[5];			
				s=ScaleBaseSide(P1,P2,P_Old,point,false,0);			
			}
			if (MousePick==2 || MousePick==3 || MousePick==4)
			{
				P_Old=CtrlPoint[3];
				P1=CtrlPoint[6];
				P2=CtrlPoint[7];
				if((nFlags & MK_SHIFT)==0) 
					s=0;
				s=ScaleBaseSide(P1,P2,P_Old,point,true,s);	
			}			
			if (MousePick==4 || MousePick==5 || MousePick==6)
			{
				P_Old=CtrlPoint[5];
				P1=CtrlPoint[0];
				P2=CtrlPoint[1];
				if((nFlags & MK_SHIFT)==0) 
					s=0;
				s=ScaleBaseSide(P1,P2,P_Old,point,false,s);					
			}
			if (MousePick==6 || MousePick==7 || MousePick==0)
			{
				P_Old=CtrlPoint[7];
				P1=CtrlPoint[2];
				P2=CtrlPoint[3];
				if((nFlags & MK_SHIFT)==0) 
					s=0;
				ScaleBaseSide(P1,P2,P_Old,point,true,s);								
			}
			CalcRotatePoint();
			break;
		case 8://rotate
			theta=CalcTheta(
				CtrlPoint[8].x-CtrlPoint[9].x,CtrlPoint[8].y-CtrlPoint[9].y,
				point.x-CtrlPoint[9].x,point.y-CtrlPoint[9].y);	
			if((nFlags & MK_SHIFT)!=0)
			{//如果SHIFT按下，7.5度的整数倍
				int ia=(int)(theta*10);
				ia=ia / 75 * 75;
				theta=ia;
				theta/=10.0;
			}
			trans.Rotate(theta,CtrlPoint[9]);
			break;
		case 9://move
			trans.Translate(point.x-MouseOldPos.x,point.y-MouseOldPos.y);
			break;
		}

		MouseOldPos=point;
		Invalidate(FALSE);
	} else
		dragging=false;
	CView::OnMouseMove(nFlags, point);
}
BOOL CTestView::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message) 
{
	/*
	IDC_ARROW 标准的箭头
	IDC_CROSS 十字光标
	IDC_HELP 标准的箭头和问号
	IDC_IBEAM 工字光标
	IDC_NO 禁止圈
	IDC_SIZEALL 四向箭头指向东、西、南、北
	IDC_SIZENESW 双箭头指向东北和西南
	IDC_SIZENS 双箭头指向南北
	IDC_SIZENWSE 双箭头指向西北和东南
	IDC_SIZEWE 双箭头指向东西
	IDC_UPARROW 垂直箭头
	*/
	//TODO: Add your message handler code here and/or call default
	if(MousePick>-1)
	{
		HCURSOR hCur;
		if (MousePick==0 || MousePick==4)
			hCur=LoadCursor( NULL,IDC_SIZENWSE ) ;
		else
		if (MousePick==2 || MousePick==6)
			hCur=LoadCursor( NULL,IDC_SIZENESW ) ;
		else
		if (MousePick==1 || MousePick==5)
			hCur=LoadCursor( NULL,IDC_SIZENS ) ;
		else
		if (MousePick==3 || MousePick==7)
			hCur=LoadCursor( NULL,IDC_SIZEWE ) ;
		else
		if (MousePick==8)
			hCur=LoadCursor( NULL,IDC_CROSS) ;
		else if (MousePick==9)
			hCur=LoadCursor( NULL,IDC_SIZEALL) ;
		else
			return false;
		SetCursor(hCur);
		return false;
	}
	return CView::OnSetCursor(pWnd, nHitTest, message);
}

void CTestView::OnLButtonDown(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	MouseOldPos=DPtoLP(point);
	BackData();
	CView::OnLButtonDown(nFlags, point);
}

void CTestView::OnLButtonUp(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	dragging=false;
	CView::OnLButtonUp(nFlags, point);
}

void CTestView::BackData()
{
	int i;	
	for(i=0;i<POINT_NUM;i++)
	{
		P_BK[i]=P[i];
	}
	for(i=0;i<10;i++)
	{
		CtrlPoint_BK[i]=CtrlPoint[i];
	}
}
void CTestView::RestoreData()
{
	int i;	
	for(i=0;i<POINT_NUM;i++)
	{
		P[i]=P_BK[i];
	}
	for(i=0;i<10;i++)
	{
		CtrlPoint[i]=CtrlPoint_BK[i];
	}
}

void CTestView::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	// TODO: Add your message handler code here and/or call default
	if (nChar==27) //ESC
	{
		MousePick=-1;
		RestoreData();
		Invalidate(false);
	} 
	if (SHIFT_DOWN==false && MousePick>=0 && MousePick<=7)
	{//如果以前SHIFT没有按下,并且正在缩放
		if (nFlags | MK_SHIFT!=0)
		{//如果现在SHIFT按下
			RestoreData();
			Invalidate(false);
			SHIFT_DOWN=true;
			return;
		}
	}
	CView::OnKeyDown(nChar, nRepCnt, nFlags);
}

void CTestView::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	// TODO: Add your message handler code here and/or call default
	if(SHIFT_DOWN)
	{
		if (nFlags | MK_SHIFT==0)
		{
			SHIFT_DOWN=false;
			Invalidate(false);
		}
	}
	CView::OnKeyUp(nChar, nRepCnt, nFlags);
}
