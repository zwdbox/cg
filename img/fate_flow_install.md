# fate_flow安装

- 本安装手册是[中文版本安装教程](https://github.com/FederatedAI/FATE/blob/master/cluster-deploy/doc/Fate-allinone_deployment_guide_install_zh.md)的补充。
- 安装前：
  - 202.115.82.8 deep02  正常
  - 202.115.82.29 deep01 有错，本次目标是要正常安装deep01
- 基本思路
  - 配置文件：/data/projects/fate-cluster-install/allInone/conf/setup.conf 两台计算机必须相同
  - 不要在/etc/profile中设置单独的环境变量，使用/data/projects/fate/init_env.sh设置即可
  - 全程使用app用户启动服务，不使用root用户

tar xzf fate-cluster-install-1.4.5-release-c7-u18.tar.gz
scp deep02:/data/projects/fate-cluster-install/allInone/conf/setup.conf /data/projects/fate-cluster-install/allInone/conf/setup.conf
setup.conf

cd /data/projects/fate-cluster-install/allInone
nohup sh ./deploy.sh > logs/boot.log 2>&1 &
tail -f ./logs/deploy.log

init over
deploy host mysql 202.115.82.8  over
deploy guest mysql  202.115.82.29  over
deploy host 202.115.82.8  over
deploy guest 202.115.82.29  over

(venv) [app@deep01 fate_flow]$ which python
/home/deep/projects/fate/common/python/venv/bin/python

## 安装python依赖包

- [国内pip镜像](https://www.cnblogs.com/schut/p/10410087.html)
- 升级pip: pip install --upgrade pip

```text
pip install requests_toolbelt
pip install grpcio
pip install grpcio-tools
pip install peewee
pip install apsw
pip install kazoo
pip install psutil
pip install deprecated
pip install numpy
pip install redis
pip install pymysql
pip install ruamel.yaml
pip install python-dotenv
pip install requests
pip install Cython
pip install google.api
pip install pandas
pip install scipy
pip install statsmodels
pip install appier
pip install pyyaml
pip install torch
pip install cachetools
pip install numba
pip install flask
pip install lmdb
pip install pycryptodomex
pip install -U scikit-learn==0.23
```

- 不能安装这个：pip install sklearn，以这个代替，只能是0.22版本，不能是高版本：scikit-learn==0.22

## 3. 安装MPFR
v=6.1.2
cd $HOME/src
wget https://gmplib.org/download/gmp/gmp-${v}.tar.bz2
tar -jxvf gmp-${v}.tar.bz2 && cd gmp-${v}
./configure --prefix=$HOME/static --enable-static --disable-shared --with-pic
make && sudo make install


## 4. 安装MPFR
v=4.1.0
cd $HOME/src
wget https://www.mpfr.org/mpfr-current/mpfr-${v}.tar.bz2
tar -jxvf mpfr-${v}.tar.bz2 && cd mpfr-${v}
./configure --prefix=$HOME/static --enable-static --disable-shared --with-pic --with-gmp=$HOME/static
make && make check && make install

## 6 安装gmpy2

- 很重要的参考：https://www.cnblogs.com/pcat/p/5746821.html
- yum install python-devel
- wget https://github.com/aleaxit/gmpy/releases/download/gmpy2-2.1.0b5/gmpy2-2.1.0b5.tar.gz

[app@deep01 allInone]$ nohup sh ./deploy.sh > logs/boot.log 2>&1 &
[app@deep01 allInone]$ tail -f ./logs/deploy.log
init over
deploy guest mysql  202.115.82.8  over
deploy guest 202.115.82.8  over
deploy host mysql 202.115.82.29  over
deploy host 202.115.82.29  over


## 启动fate_flow的严重错误

```text
cd /data/projects/fate/python/fate_flow
sh service.sh start
cat /home/deep/projects/fate/python/logs/error.log

- 错误：ModuleNotFoundError: No module named 'eggroll.api'
```

- 解决方法，复制deep02中的python目录到deep01，复制前先备份python目录

- [比较两个目录的不同](https://www.cnblogs.com/f-ck-need-u/p/9071033.html)
- cd /data/projects/fate/common
- vimdiff <(cd python; find . | sort) <(cd python_good; find . | sort)

```text
cd /data/projects/fate/common/
cp -rf python/ python_bak
scp -r deep02:/data/projects/fate/common/python /data/projects/fate/common/
```

vi /data/projects/fate/common/python/venv/bin/activate
VIRTUAL_ENV='/data/projects/fate/common/python/venv'

env $PATH
env: /data/projects/fate/common/python/venv/bin

## 启动所有服务

```text
source /data/projects/fate/init_env.sh
cd /data/projects/fate/eggroll
sh ./bin/eggroll.sh all start
sh ./bin/eggroll.sh clustermanager start

cd /data/projects/fate/python/fate_flow
sh service.sh start


cd /data/projects/fate/common/mysql/mysql-8.0.13
sh ./service.sh start

cd /data/projects/fate/fateboard
sh service.sh start
```

source /data/projects/fate/init_env.sh
cd /data/projects/fate/python/examples/toy_example/
python run_toy_example.py 9999 9999 1

## 停止所有服务pwd

```text
source /data/projects/fate/init_env.sh
cd /data/projects/fate/eggroll
sh ./bin/eggroll.sh all stop
sh ./bin/eggroll.sh clustermanager stop

cd /data/projects/fate/python/fate_flow
sh service.sh stop

cd /data/projects/fate/common/mysql/mysql-8.0.13
sh ./service.sh stop

cd /data/projects/fate/fateboard
sh service.sh start
```

## centOS 6.5关闭防火墙步骤

关闭命令：  service iptables stop
永久关闭防火墙：chkconfig iptables off

## 关闭Selinux

sudo setenforce 0
sudo getenforce

strings /lib64/libc.so.6 |grep GLIBC_

上传预设数据：
source /data/projects/fate/init_env.sh
cd /data/projects/fate/python/examples/scripts/
python upload_default_data.py -m 1

快速模式：
source /data/projects/fate/init_env.sh
cd /data/projects/fate/python/examples/min_test_task/
python run_task.py -m 1 -gid 9999 -hid 10000 -aid 10000 -f fast

python run_task.py -m 1 -gid 9999 -hid 10000 -aid 10000 -f normal

线程局部存储TLS(thread local storage)

File "/data/projects/fate/common/miniconda3/lib/python3.6/ctypes/__init__.py", line 348, in __init__
    self._handle = _dlopen(self._name, mode)
OSError: dlopen: cannot load any more object with static TLS

问题原因：程序引导太多使用了 静态TLS 的动态库，而linux 系统对于进程可加载的TLS 库，有限制
解决方法：方法1、查出哪些库使用了 静态TLS ，并减少这些库的加载，使用其他方式去加载
方法2、增加系统的限制数，可以多加载一些库，不同的操作系统版本和glibc 的版本的限制数都会不同