deep01 202.115.82.29
deep02 202.115.82.8
机型：Huawei RH2288H V3

CentOS 7 x8
[root@deep01 /]# df -h
Filesystem            Size  Used Avail Use% Mounted on
/dev/mapper/vg_deep2-lv_root
                       50G   43G  6.0G  88% /
tmpfs                  48G   18M   48G   1% /dev/shm
/dev/sda1             477M   67M  386M  15% /boot
/dev/mapper/vg_deep2-lv_home
                      1.9T  239G  1.5T  14% /home
/dev/sdd4              59G  4.5G   55G   8% /media/CentOS 7 x8


[root@deep01 oracle]# fdisk -l

Disk /dev/sdb: 480.1 GB, 480103981056 bytes
255 heads, 63 sectors/track, 58369 cylinders
Units = cylinders of 16065 * 512 = 8225280 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disk identifier: 0x0007b3fb

   Device Boot      Start         End      Blocks   Id  System
/dev/sdb1               1       58370   468849664   8e  Linux LVM

Disk /dev/sda: 600.1 GB, 600127266816 bytes
255 heads, 63 sectors/track, 72961 cylinders
Units = cylinders of 16065 * 512 = 8225280 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disk identifier: 0x982c4f37

   Device Boot      Start         End      Blocks   Id  System
/dev/sda1   *           1          64      512000   83  Linux
Partition 1 does not end on cylinder boundary.
/dev/sda2              64       72962   585547776   8e  Linux LVM

Disk /dev/sdc: 1000.2 GB, 1000204886016 bytes
255 heads, 63 sectors/track, 121601 cylinders
Units = cylinders of 16065 * 512 = 8225280 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disk identifier: 0x000835e2

   Device Boot      Start         End      Blocks   Id  System
/dev/sdc1               1      121602   976760832   8e  Linux LVM

Disk /dev/mapper/vg_deep2-lv_root: 53.7 GB, 53687091200 bytes
255 heads, 63 sectors/track, 6527 cylinders
Units = cylinders of 16065 * 512 = 8225280 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disk identifier: 0x00000000


Disk /dev/mapper/vg_deep2-lv_swap: 4294 MB, 4294967296 bytes
255 heads, 63 sectors/track, 522 cylinders
Units = cylinders of 16065 * 512 = 8225280 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disk identifier: 0x00000000


Disk /dev/mapper/vg_deep2-lv_home: 2021.9 GB, 2021914574848 bytes
255 heads, 63 sectors/track, 245817 cylinders
Units = cylinders of 16065 * 512 = 8225280 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disk identifier: 0x00000000


Disk /dev/sdd: 62.9 GB, 62914560000 bytes
255 heads, 63 sectors/track, 7648 cylinders
Units = cylinders of 16065 * 512 = 8225280 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disk identifier: 0xcad4ebea

   Device Boot      Start         End      Blocks   Id  System
/dev/sdd4   *           1        7649    61439872    c  W95 FAT32 (LBA)
[root@deep01 oracle]# 

安装tesla k40m:
https://blog.csdn.net/weixin_42545878/article/details/94735192

下载驱动：
https://www.nvidia.cn/Download/index.aspx?lang=cn

>>>关闭防火墙

systemctl stop firewalld.service            #停止firewall
systemctl disable firewalld.service        #禁止firewall开机启动



#vi /etc/modprobe.d/blacklist.conf
输入两行：
blacklist nouveau
options nouveau modeset=0
然后：
dracut –force
3.3 CentOS切换到完整多用户模式
#vi /etc/inittab
将id:5:initdefault:   GUI X11模式 ,改成：
id:3:initdefault:   完整多用户模式
重启CentOS系统




# multi-user.target: analogous to runlevel 3
# graphical.target: analogous to runlevel 5
#
# To view current default target, run:
# systemctl get-default
#
# To set a default target, run:
# systemctl set-default TARGET.target
systemctl set-default multi-user.target
systemctl set-default graphical.target

sudo sh NVIDIA-Linux-x86_64-450.80.02.run -no-x-check -no-nouveau-check -no-opengl-files

安装驱动后测试：
nvidia-smi
+-----------------------------------------------------------------------------+
| NVIDIA-SMI 450.80.02    Driver Version: 450.80.02    CUDA Version: 11.0     |
|-------------------------------+----------------------+----------------------+
| GPU  Name        Persistence-M| Bus-Id        Disp.A | Volatile Uncorr. ECC |
| Fan  Temp  Perf  Pwr:Usage/Cap|         Memory-Usage | GPU-Util  Compute M. |
|                               |                      |               MIG M. |
|===============================+======================+======================|
|   0  Tesla K40m          Off  | 00000000:81:00.0 Off |                    0 |
| N/A   23C    P8    20W / 235W |      0MiB / 11441MiB |      0%      Default |
|                               |                      |                  N/A |
+-------------------------------+----------------------+----------------------+

+-----------------------------------------------------------------------------+
| Processes:                                                                  |
|  GPU   GI   CI        PID   Type   Process name                  GPU Memory |
|        ID   ID                                                   Usage      |
|=============================================================================|
|  No running processes found                                                 |
+-----------------------------------------------------------------------------+

CentOS 7运行yum出错：Cannot find a valid baseurl for repo: base/7/x86_64:
https://blog.csdn.net/xm393392625/article/details/82144881

I/O size (minimum/optimal): 512 bytes / 512 bytes

vncserver:
https://blog.51cto.com/hwg1227/2161765
自定义端口号:
vi /usr/bin/vncserver

https://my.oschina.net/u/136074/blog/536488


## vncserver

- 以下都是在root下执行

```text
cp /lib/systemd/system/vncserver@.service /etc/systemd/system/vncserver@:1.service
cp /lib/systemd/system/vncserver@.service /etc/systemd/system/vncserver@:2.service

vim /etc/systemd/system/vncserver@:1.service
vim /etc/systemd/system/vncserver@:2.service

上面的vim修改vncserver运行的用户，比如指定两个用户deep和app：
ExecStart=/usr/bin/vncserver_wrapper deep %i
ExecStart=/usr/bin/vncserver_wrapper app %i

- 自定义端口号:
vi /usr/bin/vncserver


- 启动vncserver：
- 必须从root启动
systemctl daemon-reload
service vncserver@:1 start
service vncserver@:2 start
```

开机启动：
systemctl enable vncserver@:1.service
systemctl enable vncserver@:2.service
