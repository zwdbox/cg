// TestView.cpp : implementation of the CTestView class
//

#include "stdafx.h"
#include "Test.h"

#include "TestDoc.h"
#include "TestView.h"
#include <math.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define POINTS 5
/////////////////////////////////////////////////////////////////////////////
// CTestView

IMPLEMENT_DYNCREATE(CTestView, CView)

BEGIN_MESSAGE_MAP(CTestView, CView)
	//{{AFX_MSG_MAP(CTestView)
	ON_WM_MOUSEWHEEL()
	ON_WM_LBUTTONDOWN()
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONUP()
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTestView construction/destruction

CTestView::CTestView()
{
	// TODO: add construction code here
	m_Scale=1.0;
	leftdown=false;
}

CTestView::~CTestView()
{
}

double round(double r)
{
    return (r > 0.0) ? floor(r + 0.5) : ceil(r - 0.5);
}

BOOL CTestView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CTestView drawing

void CTestView::OnDraw(CDC* pDC)
{
	CTestDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);

	// TODO: add draw code for native data here
	CRect rect;
	GetClientRect(&rect);
	
	pDC->SetMapMode(MM_ANISOTROPIC);
	pDC->SetWindowExt(rect.Width()*m_Scale,-rect.Height()*m_Scale);
	pDC->SetViewportExt(rect.Width(),rect.Height());
	pDC->SetViewportOrg(rect.Width()/2,rect.Height()/2);
	//pDC->SetWindowOrg(-rect.Width()/2,rect.Height()/2);  
    //rect.OffsetRect(-rect.Width()/2,-rect.Height()/2);
	//pDC->Rectangle(rect);

	rect.OffsetRect(rect.Width()/2,-rect.Height()/2);	

	/*
	CBitmap Bitmap;
	Bitmap.LoadBitmap(IDB_INSTITUTE);
	CBrush NewBrush,*pOldBrush;
	NewBrush.CreatePatternBrush(&Bitmap);
	pOldBrush=pDC->SelectObject(&NewBrush);
	CPen  *pOldPen;
	pOldPen=(CPen*)pDC->SelectStockObject(NULL_PEN);
	CPoint ld,rt,sp,ep;//左下角点、右上角的、起点和终点
	ld=CPoint(-400,-600);
	rt=CPoint(400,200);
    //注意起点与终点坐标的顺序
	sp=CPoint(400,0),ep=CPoint(-400,0);
	pDC->Pie(CRect(ld,rt),sp,ep);	
	pDC->SelectObject(pOldBrush);
	Bitmap.DeleteObject();
	
	ld=CPoint(-80,-280),rt=CPoint(80,-120);
	sp=CPoint(400,0),ep=CPoint(-400,0);
	pDC->Pie(CRect(ld,rt),sp,ep);
	pDC->SelectObject(pOldPen);
	*/
	
	CPoint p[POINTS];
	CPen penBlue(PS_SOLID,5,RGB(0,0,255));
	CPen *pOldPen=pDC->SelectObject(&penBlue);
	CBrush brushRed(RGB(255,0,0));//定义红色画刷
	CBrush *pOldBrush=pDC->SelectObject(&brushRed);
	pDC->SetPolyFillMode(WINDING);//设置填充模式ALTERNATE,WINDING
	int r=200;//正五边形外接圆半径
	double Beta,Alpha;
	Beta=2.0*3.1415926/POINTS;
	Alpha=0;
	double dx,dy;
	dx=0;
	dy=0;
	if(leftdown==true)
	{
		dx=p2.x-p1.x;
		dy=p2.y-p1.y;
	}
	for(int i=0;i<POINTS;i++)
	{
		p[i].x=(long)round(r*cos(i*Beta+Alpha))+dx;
		p[i].y=(long)round(r*sin(i*Beta+Alpha))-dy;
	}
	CPoint v[POINTS];//定义五角星顶点数组

	for(i=0;i<POINTS;i++)
	{
		v[i]=p[i];
	}
	v[0]=p[0];v[1]=p[2];v[2]=p[4];v[3]=p[1];v[4]=p[3];//转储
	//v[0]=p[0];v[1]=p[1];v[2]=p[2];v[3]=p[3];v[4]=p[4];//转储
	pDC->Polygon(v,POINTS);//绘制五角星
	pDC->SelectObject(pOldPen);//恢复画笔
	pDC->SelectObject(pOldBrush);//恢复画刷

	/*
	pDC->SetTextColor(RGB(255,0,0));
	pDC->SetBkColor(RGB(255,255,0));
	pDC->TextOut(-200,20,"Compute Graphics Based on  VC++!");//输出文本1
	CString str="BoChuang Research Institute";
	CString data;
	data.Format("%s",str);//输出文本2
	pDC->TextOut(50,20,data);
	pDC->SetTextColor(RGB(0,0,0));
	pDC->SetBkMode(TRANSPARENT);
	int i1=5,i2=8;
	double d1=5.2,d2=8.3;	
	data.Format("%d,%d",i1,i2);//输出整数
	pDC->TextOut(-200,-20,data);
	data.Format("%f,%f",d1,d2);//输出小数
	pDC->TextOut(0,-20,data);
	*/
}

/////////////////////////////////////////////////////////////////////////////
// CTestView printing

BOOL CTestView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CTestView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CTestView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

/////////////////////////////////////////////////////////////////////////////
// CTestView diagnostics

#ifdef _DEBUG
void CTestView::AssertValid() const
{
	CView::AssertValid();
}

void CTestView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CTestDoc* CTestView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CTestDoc)));
	return (CTestDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CTestView message handlers

BOOL CTestView::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt) 
{
	// TODO: Add your message handler code here and/or call default
	m_Scale += 0.01 * zDelta / 120;
    //_DealShowImage();
    Invalidate();
	return CView::OnMouseWheel(nFlags, zDelta, pt);
}

void CTestView::OnLButtonDown(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	leftdown=true;
	p1=point;
	CView::OnLButtonDown(nFlags, point);
}

void CTestView::OnMouseMove(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	if (leftdown==true)
	{
		p2=point;
		Invalidate();//刷新
	}
	CView::OnMouseMove(nFlags, point);
}

void CTestView::OnLButtonUp(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	leftdown=false;
	CView::OnLButtonUp(nFlags, point);
}
