// TestView.cpp : implementation of the CTestView class
//

#include "stdafx.h"
#include "Test.h"
#include "TestDoc.h"
#include "TestView.h"
#define ROUND(a) int(a+0.5)//四舍五入
#define PI 3.1415926//圆周率
#include "cmath"//数学头文件

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// CTestView

IMPLEMENT_DYNCREATE(CTestView, CView)

BEGIN_MESSAGE_MAP(CTestView, CView)
	//{{AFX_MSG_MAP(CTestView)
	ON_COMMAND(ID_MENUPlay, OnMENUPlay)
	ON_WM_TIMER()
	ON_UPDATE_COMMAND_UI(ID_MENUPlay, OnUpdateMENUPlay)
	ON_WM_KEYDOWN()
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTestView construction/destruction

CTestView::CTestView()
{
	// TODO: add construction code here
	Play=FALSE;
	Afa=50;
	Beta=-30;
}

CTestView::~CTestView()
{
}

BOOL CTestView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CTestView drawing

void CTestView::OnDraw(CDC* pDC)
{
	CTestDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	// TODO: add draw code for native data here	
	ReadPoint();
	ReadFace();
	Rotate();
	DoubleBuffer();	
}

/////////////////////////////////////////////////////////////////////////////
// CTestView printing

BOOL CTestView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CTestView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CTestView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

/////////////////////////////////////////////////////////////////////////////
// CTestView diagnostics

#ifdef _DEBUG
void CTestView::AssertValid() const
{
	CView::AssertValid();
}

void CTestView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CTestDoc* CTestView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CTestDoc)));
	return (CTestDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CTestView message handlers

void CTestView::ReadPoint()//点表
{
	double a=130;//正方体边长为2a
	//顶点的三维坐标(x,y,z)
	POld[0].x=-a;POld[0].y=-a;POld[0].z=-a;POld[0].c=CRGB(1.0,0.0,0.0);
	POld[1].x= a;POld[1].y=-a;POld[1].z=-a;POld[1].c=CRGB(0.0,1.0,0.0);
	POld[2].x= a;POld[2].y= a;POld[2].z=-a;POld[2].c=CRGB(0.0,0.0,1.0);
	POld[3].x=-a;POld[3].y= a;POld[3].z=-a;POld[3].c=CRGB(1.0,1.0,0.0);
	POld[4].x=-a;POld[4].y=-a;POld[4].z= a;POld[4].c=CRGB(1.0,0.0,1.0);
	POld[5].x= a;POld[5].y=-a;POld[5].z= a;POld[5].c=CRGB(0.0,0.0,0.0);
	POld[6].x= a;POld[6].y= a;POld[6].z= a;POld[6].c=CRGB(1.0,1.0,1.0);
	POld[7].x=-a;POld[7].y= a;POld[7].z= a;POld[7].c=CRGB(0.0,1.0,1.0);
}
void CTestView::ReadFace()//面表
{
	//面的边数、面的顶点编号
	F[0].SetEN(4);F[0].p[0]=0;F[0].p[1]=3;F[0].p[2]=2;F[0].p[3]=1;//后面
	F[1].SetEN(4);F[1].p[0]=4;F[1].p[1]=5;F[1].p[2]=6;F[1].p[3]=7;//前面
	F[2].SetEN(4);F[2].p[0]=0;F[2].p[1]=4;F[2].p[2]=7;F[2].p[3]=3;//左面
	F[3].SetEN(4);F[3].p[0]=1;F[3].p[1]=2;F[3].p[2]=6;F[3].p[3]=5;//右面
	F[4].SetEN(4);F[4].p[0]=3;F[4].p[1]=7;F[4].p[2]=6;F[4].p[3]=2;//顶面
	F[5].SetEN(4);F[5].p[0]=0;F[5].p[1]=1;F[5].p[2]=5;F[5].p[3]=4;//底面
}

void CTestView::Rotate()//绕x轴旋转Afa,绕y轴旋转Beta
{
	for(int nVertex=0;nVertex<8;nVertex++)//正方体有8个顶点
	{
		CP3	PTemp;
		PTemp.x=POld[nVertex].x;
		PTemp.y=POld[nVertex].y*cos(PI*Afa/180)-POld[nVertex].z*sin(PI*Afa/180);
		PTemp.z=POld[nVertex].y*sin(PI*Afa/180)+POld[nVertex].z*cos(PI*Afa/180);
		PTemp.c=POld[nVertex].c;
		PNew[nVertex].x=PTemp.z*sin(PI*Beta/180)+PTemp.x*cos(PI*Beta/180);
		PNew[nVertex].y=PTemp.y;
		PNew[nVertex].z=PTemp.z*cos(PI*Beta/180)-PTemp.x*sin(PI*Beta/180);
		PNew[nVertex].c=PTemp.c;
	}
}

void CTestView::DoubleBuffer()//双缓冲绘图
{
	CRect Rect;
	GetClientRect(&Rect);
	CDC *pDC=GetDC();
	pDC->SetMapMode(MM_ANISOTROPIC);//自定义坐标系
	pDC->SetWindowExt(Rect.Width(),Rect.Height());
	pDC->SetViewportExt(Rect.Width(),-Rect.Height());//x轴水平向右，y轴垂直向上
	pDC->SetViewportOrg(Rect.right/2,Rect.bottom/2);//屏幕中心为原点	
	CDC	MemDC,Picture;
	MemDC.CreateCompatibleDC(pDC);
	MemDC.SetMapMode(MM_ANISOTROPIC);
	MemDC.SetWindowExt(Rect.Width(),Rect.Height());
	MemDC.SetViewportExt(Rect.Width(),-Rect.Height());
	MemDC.SetViewportOrg(Rect.Width()/2,Rect.Height()/2);
	CBitmap NewBitmap,*OldBitmap;
	NewBitmap.LoadBitmap(IDB_BITMAP2);
	OldBitmap=MemDC.SelectObject(&NewBitmap);
	MemDC.BitBlt(-Rect.Width()/2,-Rect.Height()/2,Rect.Width(),Rect.Height(),&Picture,-Rect.Width()/2,-Rect.Height()/2,SRCCOPY);
	DrawObject(&MemDC);
	pDC->BitBlt(-Rect.Width()/2,-Rect.Height()/2,Rect.Width(),Rect.Height(),&MemDC,-Rect.Width()/2,-Rect.Height()/2,SRCCOPY);
	ReleaseDC(pDC);
	MemDC.SelectObject(OldBitmap);	
	NewBitmap.DeleteObject();
}

void CTestView::DrawObject(CDC* pDC)
{
	CP3 ScreenP,t;	
	CLine line;
	for(int nFace=0;nFace<6;nFace++)
	{
		for(int nEdge=0;nEdge<F[nFace].En;nEdge++)//边循环
		{
			ScreenP=PNew[F[nFace].p[nEdge]];
			if(nEdge==0)
			{
				line.MoveTo(pDC,ROUND(ScreenP.x),ROUND(ScreenP.y),ScreenP.c);
				t=ScreenP;
			}
			else
			{
				line.LineTo(pDC,ROUND(ScreenP.x),ROUND(ScreenP.y),ScreenP.c);
			}		
		}
		line.LineTo(pDC,ROUND(t.x),ROUND(t.y),t.c);//闭合四边形
	}		
}


void CTestView::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	// TODO: Add your message handler code here and/or call default
	if(!Play)
	{
		switch(nChar)
		{
		case VK_UP:
			Afa-=1;//设定步长
			break;
		case VK_DOWN:
			Afa+=1;
			break;
		case VK_LEFT:
			Beta-=1;
			break;
		case VK_RIGHT:
			Beta+=1;
			break;
		default:
			break;			
		}		
	}
	Rotate();
	DoubleBuffer();		
	CView::OnKeyDown(nChar, nRepCnt, nFlags);
}

void CTestView::OnMENUPlay()//动画按钮函数 
{
	// TODO: Add your command handler code here
	Play=Play?FALSE:TRUE;
	if (Play)//设置动画的时间步
		SetTimer(1,15,NULL);		
	else
		KillTimer(1);
}

void CTestView::OnTimer(UINT nIDEvent)//动画时间函数 
{
	// TODO: Add your message handler code here and/or call default
	Afa-=1,Beta-=1;
	Rotate();
	DoubleBuffer();
	CView::OnTimer(nIDEvent);
}

void CTestView::OnUpdateMENUPlay(CCmdUI* pCmdUI) //动画按钮状态函数 
{
	// TODO: Add your command update UI handler code here
	if(Play)
		pCmdUI->SetCheck(TRUE);
	else
		pCmdUI->SetCheck(FALSE);	
}


