// fillpolyDoc.h : interface of the CFillpolyDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_FILLPOLYDOC_H__60AFB57D_305E_4B80_A27C_0615EA3F0435__INCLUDED_)
#define AFX_FILLPOLYDOC_H__60AFB57D_305E_4B80_A27C_0615EA3F0435__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CFillpolyDoc : public CDocument
{
protected: // create from serialization only
	CFillpolyDoc();
	DECLARE_DYNCREATE(CFillpolyDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFillpolyDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CFillpolyDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CFillpolyDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FILLPOLYDOC_H__60AFB57D_305E_4B80_A27C_0615EA3F0435__INCLUDED_)
