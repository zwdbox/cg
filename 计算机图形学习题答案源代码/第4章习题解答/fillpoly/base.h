#include "stdafx.h"


/*多边形*/
typedef struct POLYGON{
	POINT *point;
	int 	*pntnum;
	int 	plynum;
}POLYGON;

/*多边形边表*/
typedef struct LINE{
	int y;	/*边所交的最高扫描线号(顶点的最大y值)*/
	double x;/*当前扫描线与边的交点x值*/
	double dx; /*从当前扫描线到下一扫描线之间的x增量*/
	int dy;/*边的两个顶点的y差值>=0*/
	struct LINE *next;/*下一条边*/
}LINE;

/*Y桶表*/
typedef struct Y_TUB{
	int	y; /*该桶最大值*/
	struct Y_TUB	*next;/*下一桶*/
	struct LINE	*line;/*该桶的边表*/
}Y_TUB;

/*活性边表*/
typedef struct EXP_LINE{
	int y;/*当前扫描线*/
	struct LINE *line;/*活性边*/
}EXP_LINE;

int DrawPolygon(HDC hDC,POLYGON *poly);
