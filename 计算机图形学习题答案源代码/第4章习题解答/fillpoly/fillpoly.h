// fillpoly.h : main header file for the FILLPOLY application
//

#if !defined(AFX_FILLPOLY_H__4DBF4E0E_FDE9_43DD_AC3B_E2B57ABED79A__INCLUDED_)
#define AFX_FILLPOLY_H__4DBF4E0E_FDE9_43DD_AC3B_E2B57ABED79A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CFillpolyApp:
// See fillpoly.cpp for the implementation of this class
//

class CFillpolyApp : public CWinApp
{
public:
	CFillpolyApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFillpolyApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CFillpolyApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FILLPOLY_H__4DBF4E0E_FDE9_43DD_AC3B_E2B57ABED79A__INCLUDED_)
