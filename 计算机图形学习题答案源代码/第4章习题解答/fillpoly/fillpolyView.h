// fillpolyView.h : interface of the CFillpolyView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_FILLPOLYVIEW_H__C9F39126_0F35_4A9E_9FAA_74C3306D4994__INCLUDED_)
#define AFX_FILLPOLYVIEW_H__C9F39126_0F35_4A9E_9FAA_74C3306D4994__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CFillpolyView : public CView
{
protected: // create from serialization only
	CFillpolyView();
	DECLARE_DYNCREATE(CFillpolyView)

// Attributes
public:
	CFillpolyDoc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFillpolyView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CFillpolyView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CFillpolyView)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in fillpolyView.cpp
inline CFillpolyDoc* CFillpolyView::GetDocument()
   { return (CFillpolyDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FILLPOLYVIEW_H__C9F39126_0F35_4A9E_9FAA_74C3306D4994__INCLUDED_)
