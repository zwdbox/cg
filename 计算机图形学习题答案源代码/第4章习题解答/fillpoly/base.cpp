#include "stdafx.h"
#include "base.h"



/*交点在X方向由小到大排序*/
void SortPointsX(POINT *point,int pnum)
{
	int i,j,x;
	int change=FALSE;
	for(i=pnum-1;i>0;i--)
	{
		for(j=0;j<i;j++)
		{
			if(point[j].x>point[j+1].x)
			{
				x=point[j].x;
				point[j].x=point[j+1].x;
				point[j+1].x=x;
				change=TRUE;
			}
		}
		if (change==FALSE) break;
	}
}
/*连接交点序列*/
void DrawPoints(HDC hDC,POINT *point,int pnum)
{
	int i;
	int x1,y1,x2,y2;
	for(i=0;i<pnum;i+=2)
	{
		x1=point[i].x;
		y1=point[i].y;
		x2=point[i+1].x;
		y2=point[i+1].y;
		if(i==(pnum-2)) x2--; /*左闭右开*/
			if(x2<x1) break;
		MoveToEx(hDC,x1,y1,NULL);
		LineTo(hDC,x2,y2);
	}
}
/*清除活性边链表中dy=0的边*/
void ClearExpLine(EXP_LINE *ExpLine)
{
	LINE *tmpline,*prevline;
	int i;
	tmpline=ExpLine->line;
	prevline=tmpline;
	for(i=0;tmpline;i++,tmpline=tmpline->next)
	{
		if(tmpline->dy==0)
		{
			if(i==0)
			{
				ExpLine->line=tmpline->next;
				i=-1;
			}
			else
				prevline->next=tmpline->next;
		}
		else
			prevline=tmpline;
	}
}
/*临时函数，观察生成的Y桶链表*/
void PrintYTub(Y_TUB* ytub)
{
	LINE *tmpline;
	Y_TUB* tmptub;
	printf("\n");

	tmptub=ytub;
	while(tmptub)
	{
		printf("y=%d   ",tmptub->y);
		tmpline=tmptub->line;
		while(tmpline)
		{
		  printf("y=%d,x=%f,dy=%d,dx=%f  ",tmpline->y,tmpline->dx,tmpline->dy,tmpline->dx);
		  tmpline=tmpline->next;
		}
		printf("\n");
		tmptub=tmptub->next;
	}
	printf("\n");
}

void DrawYTub(HDC hDC,Y_TUB* ytub)
{
	EXP_LINE ExpLine;
	Y_TUB *tmptub;
	LINE *tmpline;//,*prevline;
	int i,c;
	POINT *point;
	if(ytub==NULL) return;
	ExpLine.y=ytub->y;/*取第一桶的y值，作为第一条扫描线，由上到下扫描*/
	ExpLine.line=ytub->line;/*取第一桶的边作为活性边表*/
	tmptub=ytub->next;/*取下一桶*/
	for(;;)
	{
		ClearExpLine(&ExpLine);/*清除活性边表中dy=0的边*/
		if(tmptub)/*判断下一桶的边是否需要加入活性边表*/
		{
			if(ExpLine.y==tmptub->y)/*需要加入*/
			{
			  tmpline=ExpLine.line;
			  while(tmpline->next) tmpline=tmpline->next;
              tmpline->next=tmptub->line; /*把下一桶的边表加到活性边表的后面*/
			  ClearExpLine(&ExpLine);/*清除活性边表中dy=0的边*/
			  tmptub=tmptub->next;
			}
		}
		if(ExpLine.line==NULL) break; //上闭下开
		tmpline=ExpLine.line;
		for(c=0;tmpline;c++) tmpline=tmpline->next; /*计算活性边表中的边数，作为交点数*/
		point=(POINT*)malloc(c*sizeof(POINT));/*分配交点*/
		tmpline=ExpLine.line;
		for(i=0;i<c;i++,tmpline=tmpline->next)/*计算交点*/
		{
			point[i].x=(int)tmpline->x;
			point[i].y=ExpLine.y;
		}
		tmpline=ExpLine.line;
		SortPointsX(point,c);/*交点在X方向排序*/
		DrawPoints(hDC,point,c);  /*连接交点序列*/
		ExpLine.y-=1;/*扫描线下移*/
		for(i=0;i<c;i++,tmpline=tmpline->next)/*更新活性边表*/
		{
			tmpline->y-=1;
			tmpline->x+=tmpline->dx;
			tmpline->dy-=1;
		}
		free(point);
	}
}
void FreeYTub(Y_TUB *ytub)
{
	Y_TUB *tmptub,*tmptub2;
	tmptub=ytub;
	while(tmptub)
	{
		tmptub2=tmptub->next;
		free(tmptub);
		tmptub=tmptub2;
	}
}
int DrawPolygon(HDC hDC,POLYGON *poly)
{
	LINE *line,*tmpline,*tmpline2;
	POINT *point,*tmppoint;
	Y_TUB *ytub,*tmptub,*tmptub2,*prevtub;
	int i,j,k,m;
	int nside=0;
	for(i=0;i<poly->plynum;i++)
	  nside+=poly->pntnum[i];
	if(nside<3) return FALSE;
	point=poly->point;
	/*分配多边形边表*/
	line=(LINE*)malloc(nside*sizeof(LINE));
	if(!line) return FALSE;
	i=0;
	tmppoint=point;

	/*填充多边形边表*/
	for(m=0;m<poly->plynum;m++) /*point_num(j) +2 while side_num(i) +1*/
		/*每个子多边形循环*/
	{
		/*子多边形每个点循环*/
		for(j=0;j<poly->pntnum[m];j++)
		{
			if(j<poly->pntnum[m]-1) k=j+1;/*如果不是最后一个点*/
			else k=0;
			line[i].x=(double)tmppoint[j].x;
			line[i].y=tmppoint[j].y;
			line[i].dy=tmppoint[j].y-tmppoint[k].y;
			if(line[i].dy<0)
			{
				line[i].x=tmppoint[k].x;
				line[i].y=tmppoint[k].y;
				line[i].dy=-line[i].dy;
			}
			if(line[i].dy)  /*如果该线不是水平线, 否则line[i].dx无意义*/
				line[i].dx=-(double)(tmppoint[k].x-tmppoint[j].x)/
								(double)(tmppoint[k].y-tmppoint[j].y);
			
			line[i].next=NULL;
			i++;
		}
		tmppoint=tmppoint+poly->pntnum[m];
	}
	/*创建Y桶链表*/
	tmpline=tmpline2=line;
	ytub=(Y_TUB*)malloc(sizeof(Y_TUB));
	if(!ytub) return FALSE;
	for(i=0;i<nside;i++,line++)
	{
/* 　   if(line->dy==0) continue;*/
		if(i==0)
		{
			ytub->y=line->y;
			ytub->next=NULL;
			ytub->line=line;
		}
		else
		{
			tmptub=ytub;
			prevtub=tmptub;
			for(j=0;;j++)
			{
				if(line->y > tmptub->y)/*需要在前插入新Y桶*/
				{
					tmptub2=(Y_TUB*)malloc(sizeof(Y_TUB));
					if(!tmptub2) return FALSE;
					tmptub2->y=line->y;
					tmptub2->next=tmptub;
					tmptub2->line=line;
					if(j==0)
						ytub=tmptub2;
					else
						prevtub->next=tmptub2;
					break;
				}
				else if(line->y == tmptub->y)/*不需创建新Y桶，只需加入边在当前Y桶*/
				{
					tmpline=tmptub->line;
					while(tmpline->next) tmpline=tmpline->next;
					tmpline->next=line;
					break;
				}
				else if(line->y < tmptub->y && tmptub->next==NULL)/*需要在后插入新Y桶*/
				{
					tmptub2=(Y_TUB*)malloc(sizeof(Y_TUB));
					if(!tmptub2) return FALSE;
					tmptub2->y=line->y;
					tmptub2->next=NULL;
					tmptub2->line=line;
					tmptub->next=tmptub2;
					break;
				}
				prevtub=tmptub;
				tmptub=tmptub->next;
			}
		}
	}/*创建Y桶链表结束*/

	DrawYTub(hDC,ytub);
	line=tmpline2;
	free(line);
	FreeYTub(ytub);
	return TRUE;
}
