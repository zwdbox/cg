// fillpolyView.cpp : implementation of the CFillpolyView class
//

#include "stdafx.h"
#include "fillpoly.h"

#include "fillpolyDoc.h"
#include "fillpolyView.h"

#include "base.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFillpolyView

IMPLEMENT_DYNCREATE(CFillpolyView, CView)

BEGIN_MESSAGE_MAP(CFillpolyView, CView)
	//{{AFX_MSG_MAP(CFillpolyView)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFillpolyView construction/destruction

CFillpolyView::CFillpolyView()
{
	// TODO: add construction code here

}

CFillpolyView::~CFillpolyView()
{
}

BOOL CFillpolyView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CFillpolyView drawing

void CFillpolyView::OnDraw(CDC* pDC)
{
	CFillpolyDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	// TODO: add draw code for native data here
	//CPaintDC dc(this); // device context for painting
	CClientDC dc(this);
	/*
	POLYGON poly;
	POINT point[7];
	int pntnum[]={4,3};
	point[0].x=100;
	point[0].y=120; 
	point[1].x=200;
	point[1].y=100;
	point[2].x=200;
	point[2].y=200;
	point[3].x=100;
	point[3].y=200;

	point[4].x=150;
	point[4].y=120;
	point[5].x=180;
	point[5].y=180;
	point[6].x=120;
	point[6].y=180;

	poly.point=point;
	poly.pntnum=pntnum;
	poly.plynum=2;
	*/
	POLYGON poly;
	POINT point[20];
	int pntnum[]={4,5,5};
	point[0].x=265;
	point[0].y=5;
	point[1].x=560;
	point[1].y=90;
	point[2].x=462;
	point[2].y=400;
	point[3].x=60;
	point[3].y=220;

	point[4].x=50;
	point[4].y=50;
	point[5].x=410;
	point[5].y=50;
	point[6].x=200;
	point[6].y=401;
	point[7].x=130;
	point[7].y=230;
	point[8].x=10;
	point[8].y=400;

	point[9].x=300;
	point[9].y=300;
	point[10].x=300;
	point[10].y=100;
	point[11].x=450;
	point[11].y=310;
	point[12].x=450;
	point[12].y=100;
	point[13].x=300;
	point[13].y=350;
	poly.point=point;
	poly.pntnum=pntnum;
	poly.plynum=3;
	
	CPen Pen,*OldPen;
	Pen.CreatePen(PS_SOLID,1,RGB(255,0,0));
	OldPen=dc.SelectObject(&Pen);
	DrawPolygon(dc.m_hDC,&poly);
	dc.SelectObject(OldPen);
}

/////////////////////////////////////////////////////////////////////////////
// CFillpolyView printing

BOOL CFillpolyView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CFillpolyView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CFillpolyView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

/////////////////////////////////////////////////////////////////////////////
// CFillpolyView diagnostics

#ifdef _DEBUG
void CFillpolyView::AssertValid() const
{
	CView::AssertValid();
}

void CFillpolyView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CFillpolyDoc* CFillpolyView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CFillpolyDoc)));
	return (CFillpolyDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CFillpolyView message handlers
