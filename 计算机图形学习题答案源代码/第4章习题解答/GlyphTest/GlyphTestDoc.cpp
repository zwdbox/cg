// GlyphTestDoc.cpp : implementation of the CGlyphTestDoc class
//

#include "stdafx.h"
#include "GlyphTest.h"

#include "GlyphTestDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CGlyphTestDoc

IMPLEMENT_DYNCREATE(CGlyphTestDoc, CDocument)

BEGIN_MESSAGE_MAP(CGlyphTestDoc, CDocument)
	//{{AFX_MSG_MAP(CGlyphTestDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CGlyphTestDoc construction/destruction

CGlyphTestDoc::CGlyphTestDoc()
{
	// TODO: add one-time construction code here

}

CGlyphTestDoc::~CGlyphTestDoc()
{
}

BOOL CGlyphTestDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CGlyphTestDoc serialization

void CGlyphTestDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CGlyphTestDoc diagnostics

#ifdef _DEBUG
void CGlyphTestDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CGlyphTestDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CGlyphTestDoc commands
