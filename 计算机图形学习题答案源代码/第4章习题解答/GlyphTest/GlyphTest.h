// GlyphTest.h : main header file for the GLYPHTEST application
//

#if !defined(AFX_GLYPHTEST_H__AF6B06C4_C43A_4375_9EFE_8BABF4E4D76F__INCLUDED_)
#define AFX_GLYPHTEST_H__AF6B06C4_C43A_4375_9EFE_8BABF4E4D76F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CGlyphTestApp:
// See GlyphTest.cpp for the implementation of this class
//

class CGlyphTestApp : public CWinApp
{
public:
	CGlyphTestApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGlyphTestApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CGlyphTestApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GLYPHTEST_H__AF6B06C4_C43A_4375_9EFE_8BABF4E4D76F__INCLUDED_)
