// GlyphTestView.h : interface of the CGlyphTestView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_GLYPHTESTVIEW_H__50B3BBE3_7FFC_4C00_A18D_251B2000209E__INCLUDED_)
#define AFX_GLYPHTESTVIEW_H__50B3BBE3_7FFC_4C00_A18D_251B2000209E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CGlyphTestView : public CView
{
protected: // create from serialization only
	CGlyphTestView();
	DECLARE_DYNCREATE(CGlyphTestView)

// Attributes
public:
	CGlyphTestDoc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGlyphTestView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CGlyphTestView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
// Generated message map functions
protected:
	//{{AFX_MSG(CGlyphTestView)
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in GlyphTestView.cpp
inline CGlyphTestDoc* CGlyphTestView::GetDocument()
   { return (CGlyphTestDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GLYPHTESTVIEW_H__50B3BBE3_7FFC_4C00_A18D_251B2000209E__INCLUDED_)
