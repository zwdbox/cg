// GlyphTestView.cpp : implementation of the CGlyphTestView class
//

#include "stdafx.h"
#include "GlyphTest.h"

#include <string.h>
#include "GlyphTestDoc.h"
#include "GlyphTestView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CGlyphTestView

IMPLEMENT_DYNCREATE(CGlyphTestView, CView)

BEGIN_MESSAGE_MAP(CGlyphTestView, CView)
	//{{AFX_MSG_MAP(CGlyphTestView)
	ON_WM_SHOWWINDOW()
	ON_WM_LBUTTONDOWN()
	ON_WM_RBUTTONDOWN()
	ON_WM_MOUSEMOVE()
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
END_MESSAGE_MAP()

// CGlyphTestView 构造/析构

#define TAGSIZE 4
int fontsize=260;//字大小
wchar_t fontname[]=L"宋体";//字体 _T("楷体")
wchar_t Text[30]=L"闻过则喜";//要显示的字，最多30个汉字
POINT Point[40000];//假定30个汉字最多有40000个控制点
BYTE LineStyle[40000];
int Index=0; //控制点数量
int SelectedIndex;//鼠标选中的控制点

bool bDispControlPolygon=true;//是否显示控制点

void AddPoint(int x, int y, BYTE Style) 
{ 
	//添加点 
	Point[Index].x=x; 
	Point[Index].y=y; 
	LineStyle[Index]=Style; 
	Index++; 
} 
void AddCSPLine(int x1, int y1, int x2, int y2, int x3, int y3) 
{ 
	//添加曲线 
	AddPoint(x1, y1, PT_BEZIERTO); 
	AddPoint(x2, y2, PT_BEZIERTO); 
	AddPoint(x3, y3, PT_BEZIERTO); 
} 
void AddQSPLine(int x1, int y1, int x2, int y2) 
{ 
	//添加曲线 
	int x0=Point[Index-1].x; 
	int y0=Point[Index-1].y; 
	AddCSPLine((x0+2*x1)/3, (y0+2*y1)/3, (2*x1+x2)/3, (2*y1+y2)/3, x2, y2); 
} 
void CreateData(CDC *pDC)
{
	//设置pDC的字体
	LOGFONT logfont;
	memset(&logfont, 0, sizeof(LOGFONT));
	logfont.lfHeight = fontsize;
	logfont.lfCharSet = GB2312_CHARSET;
	//wcscpy(logfont.lfFaceName, fontname);
	CFont font;
	font.CreateFontIndirect(&logfont);
	CFont* pOldFont = pDC->SelectObject(&font);
	
	//设置取得字体的变换矩阵
	//下面的-1表示在Y方向作镜向变换。如果为+1，则文字的Y方向是倒置的。
	MAT2 Mat = {{0,1}, {0,0}, {0,0}, {0,-1}};

	DWORD Size; 
	GLYPHMETRICS gm={0}; 
	int j,i=0;	
	DWORD returnBytes;
	TTPOLYGONHEADER* header,*oldheader;
	Index=0;
	for(i=0; i <(int)wcslen(Text); i++) 
	{//循环每个字
		Size=GetGlyphOutlineW(pDC->m_hDC, Text[i], GGO_BEZIER, &gm, NULL, NULL, &Mat); 
		if(Size!=GDI_ERROR) 
		{ 
			header=(TTPOLYGONHEADER*)malloc(Size);
			oldheader=header;
			returnBytes =GetGlyphOutlineW(pDC->m_hDC, Text[i], GGO_BEZIER, &gm, Size, header, &Mat); 
			while(returnBytes > 0)
			{//循环单个字的每一个环
				//第一点
				//x.value+fontsize*i 表示设置水平方向的字间距离
				//y.value+fontsize表示设置向下移动fontsize以便被看见，否则在上方超出屏幕
				AddPoint(header->pfxStart.x.value+fontsize*i, header->pfxStart.y.value+fontsize, PT_MOVETO); 
				//到下一点后的点列
				TTPOLYCURVE* pCurrentCurve = (TTPOLYCURVE*)(header+1);
				int remainBytes = header->cb - sizeof(TTPOLYGONHEADER);
				while(remainBytes > 0)
				{//循环环中的其余每个点
					switch(pCurrentCurve->wType) 
					{ 
						case TT_PRIM_LINE: 
							for(j=0; j <pCurrentCurve->cpfx; j++) 
								AddPoint(pCurrentCurve->apfx[j].x.value+fontsize*i, pCurrentCurve->apfx[j].y.value+fontsize, PT_LINETO); 
							break; 
						case TT_PRIM_QSPLINE: 
							if(pCurrentCurve->cpfx>=2) 
							{ 
								for(j=0; j <pCurrentCurve->cpfx-1; j++) 
								{ 
									if(i==pCurrentCurve->cpfx-2) 
										AddQSPLine(pCurrentCurve->apfx[j].x.value+fontsize*i, pCurrentCurve->apfx[j].y.value+fontsize, 
										pCurrentCurve->apfx[j+1].x.value+fontsize*i, pCurrentCurve->apfx[j+1].y.value+fontsize); 
									else 
										AddQSPLine(pCurrentCurve->apfx[j].x.value+fontsize*i, pCurrentCurve->apfx[j].y.value+fontsize, 
											((pCurrentCurve->apfx[j].x.value+fontsize*i)+(pCurrentCurve->apfx[j+1].x.value+fontsize*i))/2, 
											((pCurrentCurve->apfx[j].y.value+fontsize)+(pCurrentCurve->apfx[j+1].y.value+fontsize))/2
											);
								} 
							} 
							break; 
						case TT_PRIM_CSPLINE: 
							if((pCurrentCurve->cpfx%3)==0) 
							{ 
								for(j=0; j <pCurrentCurve->cpfx; j+=3) 
									AddCSPLine(pCurrentCurve->apfx[j].x.value+fontsize*i, pCurrentCurve->apfx[j].y.value+fontsize, 
										pCurrentCurve->apfx[j+1].x.value+fontsize*i, pCurrentCurve->apfx[j+1].y.value+fontsize, 
										pCurrentCurve->apfx[j+2].x.value+fontsize*i, pCurrentCurve->apfx[j+2].y.value+fontsize); 
							} 
							break;
					}
					int count = sizeof(TTPOLYCURVE) + (pCurrentCurve->cpfx-1)*sizeof(POINTFX);
					pCurrentCurve = (TTPOLYCURVE*)((char*)pCurrentCurve + count);
					remainBytes -= count;
				}
				//绘制到起点,闭合多边形	PT_CLOSEFIGURE			
				AddPoint(header->pfxStart.x.value+fontsize*i, header->pfxStart.y.value+fontsize, PT_LINETO|PT_CLOSEFIGURE); 

				returnBytes -= header->cb;					
				header = (TTPOLYGONHEADER*)((char*)header + header->cb);
			}// while(returnBytes > 0) 循环单个字的每一个环
			free(oldheader);
		}
	}
}
void DrawControlPolygon(CDC *pDC)
{
	for(int i=0;i<Index;i++)
	{
		if(LineStyle[i]==PT_MOVETO)
			pDC->MoveTo(Point[i]);
		else
			pDC->LineTo(Point[i]);
		if(LineStyle[i]==PT_BEZIERTO)
			pDC->Ellipse(Point[i].x-TAGSIZE,Point[i].y-TAGSIZE,Point[i].x+TAGSIZE,Point[i].y+TAGSIZE);
		else
			pDC->Rectangle(Point[i].x-TAGSIZE,Point[i].y-TAGSIZE,Point[i].x+TAGSIZE,Point[i].y+TAGSIZE);
	}
}

CGlyphTestView::CGlyphTestView()
{
	// TODO: 在此处添加构造代码

}

CGlyphTestView::~CGlyphTestView()
{
}

BOOL CGlyphTestView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: 在此处通过修改
	//  CREATESTRUCT cs 来修改窗口类或样式

	return CView::PreCreateWindow(cs);
}

// CGlyphTestView 绘制

void CGlyphTestView::OnDraw(CDC* pDC)
{
	CGlyphTestDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;
	CDC dc;
	CRect rect;
	GetClientRect(&rect);
	dc.CreateCompatibleDC(pDC);
	CBitmap bmp,*oldbmp;
	bmp.CreateCompatibleBitmap(pDC,rect.Width(),rect.Height());
	oldbmp=dc.SelectObject(&bmp);
	dc.FillRect(&rect,NULL);
	wchar_t str1[]=L"运行时，右键开/关控制多边形";
	wchar_t str2[]=L"请在源程序GlyphTestView.cpp中修改要显示的文字和字号。";
	
	dc.TextOut(0,0,str1);
	dc.TextOut(0,20,str2);
	CPen GrayPen,BlackPen;
	GrayPen.CreatePen(PS_SOLID,1,RGB(0,0,255));
	BlackPen.CreatePen(PS_SOLID,2,RGB(0,0,0));
	CPen *pOldPen;

	pOldPen=(CPen*)dc.SelectObject(&GrayPen);	
	if(bDispControlPolygon)
		DrawControlPolygon(&dc);
	dc.SelectObject(&BlackPen);
	dc.SetPolyFillMode(ALTERNATE);

	CBrush b;
	b.CreateSolidBrush(RGB(255,0,255));
	dc.SelectObject(&b);
	dc.BeginPath();
		dc.PolyDraw(Point, LineStyle, Index);
	dc.EndPath();
	dc.FillPath();

	dc.SelectObject(pOldPen);
	dc.SelectObject(&oldbmp);
	bmp.DeleteObject();
	pDC->BitBlt(0,0,rect.Width(),rect.Height(),&dc,0,0,SRCCOPY);
}


// CGlyphTestView 打印

BOOL CGlyphTestView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// 默认准备
	return DoPreparePrinting(pInfo);
}

void CGlyphTestView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 添加额外的打印前进行的初始化过程
}

void CGlyphTestView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 添加打印后进行的清理过程
}


// CGlyphTestView 诊断

#ifdef _DEBUG
void CGlyphTestView::AssertValid() const
{
	CView::AssertValid();
}

void CGlyphTestView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CGlyphTestDoc* CGlyphTestView::GetDocument() 
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CGlyphTestDoc)));
	return (CGlyphTestDoc*)m_pDocument;
}
#endif //_DEBUG


// CGlyphTestView 消息处理程序
/*
void CGlyphTestView::OnShowWindow2(BOOL bShow, UINT nStatus)
{
	CView::OnShowWindow(bShow, nStatus);
	CClientDC dc(this);
	CreateData(&dc);
	// TODO: 在此处添加消息处理程序代码
}
*/
void CGlyphTestView::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	CRect rect;
	SelectedIndex=-1;
	for(int i=0;i<Index;i++)
	{
		rect.SetRect(Point[i].x-TAGSIZE,Point[i].y-TAGSIZE,Point[i].x+TAGSIZE,Point[i].y+TAGSIZE);
		if(PtInRect(&rect,point))
		{
			SelectedIndex=i;
			break;
		}
	}
	CView::OnLButtonDown(nFlags, point);
}

void CGlyphTestView::OnMouseMove(UINT nFlags, CPoint point)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	if(SelectedIndex>=0 && (nFlags & MK_LBUTTON)!=0)
	{
		Point[SelectedIndex]=point;
		Invalidate(false);
	}
	CView::OnMouseMove(nFlags, point);
}

void CGlyphTestView::OnRButtonDown(UINT nFlags, CPoint point)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	bDispControlPolygon=!bDispControlPolygon;
	Invalidate(false);
	CView::OnRButtonDown(nFlags, point);
}

void CGlyphTestView::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	CView::OnShowWindow(bShow, nStatus);
	CClientDC dc(this);
	CreateData(&dc);	
	// TODO: Add your message handler code here
	
}
