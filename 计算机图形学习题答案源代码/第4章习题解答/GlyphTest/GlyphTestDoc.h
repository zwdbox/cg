// GlyphTestDoc.h : interface of the CGlyphTestDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_GLYPHTESTDOC_H__17D00D58_4A66_40E2_B42E_A4414CC90ABF__INCLUDED_)
#define AFX_GLYPHTESTDOC_H__17D00D58_4A66_40E2_B42E_A4414CC90ABF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CGlyphTestDoc : public CDocument
{
protected: // create from serialization only
	CGlyphTestDoc();
	DECLARE_DYNCREATE(CGlyphTestDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGlyphTestDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CGlyphTestDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CGlyphTestDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GLYPHTESTDOC_H__17D00D58_4A66_40E2_B42E_A4414CC90ABF__INCLUDED_)
