// FillDibDoc.h : interface of the CFillDibDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_FILLDIBDOC_H__1CFE6AED_E3AA_11D7_9F52_0004764652A5__INCLUDED_)
#define AFX_FILLDIBDOC_H__1CFE6AED_E3AA_11D7_9F52_0004764652A5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "image.h"
class CFillDibDoc : public CDocument
{
protected: // create from serialization only
	CFillDibDoc();
	DECLARE_DYNCREATE(CFillDibDoc)

// Attributes
public:
	CImage m_Bmp;
// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFillDibDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	virtual BOOL OnOpenDocument(LPCTSTR lpszPathName);
	virtual BOOL OnSaveDocument(LPCTSTR lpszPathName);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CFillDibDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CFillDibDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FILLDIBDOC_H__1CFE6AED_E3AA_11D7_9F52_0004764652A5__INCLUDED_)
