#if !defined(AFX_INFODLG_H__8C5F8FE1_E689_11D7_9F52_0004764652A5__INCLUDED_)
#define AFX_INFODLG_H__8C5F8FE1_E689_11D7_9F52_0004764652A5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// InfoDlg.h : header file
//

#include "image.h"
/////////////////////////////////////////////////////////////////////////////
// CInfoDlg dialog

class CInfoDlg : public CDialog
{
	int m_noldsel;
	int m_nselcolor;
// Construction
public:
	CInfoDlg(CWnd* pParent = NULL);   // standard constructor
	CImage *m_pImage;
	CPoint	m_posStart;
	CSize	m_szColor;
// Dialog Data
	//{{AFX_DATA(CInfoDlg)
	enum { IDD = IDD_INFODLG };
	CString	m_strInfo;
	//}}AFX_DATA
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CInfoDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CInfoDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_INFODLG_H__8C5F8FE1_E689_11D7_9F52_0004764652A5__INCLUDED_)
