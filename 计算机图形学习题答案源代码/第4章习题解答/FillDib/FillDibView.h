// FillDibView.h : interface of the CFillDibView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_FILLDIBVIEW_H__1CFE6AEF_E3AA_11D7_9F52_0004764652A5__INCLUDED_)
#define AFX_FILLDIBVIEW_H__1CFE6AEF_E3AA_11D7_9F52_0004764652A5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include "FillDibDoc.h"

class CFillDibView : public CView
{
protected: // create from serialization only
	CFillDibView();
	DECLARE_DYNCREATE(CFillDibView)

// Attributes
public:
	CFillDibDoc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFillDibView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CFillDibView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CFillDibView)
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnShowinfo();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in FillDibView.cpp
inline CFillDibDoc* CFillDibView::GetDocument()
   { return (CFillDibDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FILLDIBVIEW_H__1CFE6AEF_E3AA_11D7_9F52_0004764652A5__INCLUDED_)
