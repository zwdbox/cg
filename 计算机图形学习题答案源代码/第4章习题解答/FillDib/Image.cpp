// Image.cpp: implementation of the CImage class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Image.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CImage::CImage()
{
	m_hBmp=NULL;
	m_hPalette=NULL;
	m_pBits=NULL;
	m_pBmpInfo=NULL;
}

CImage::~CImage()
{
	if(m_pBmpInfo)GlobalFree(m_pBmpInfo);
	if(m_pBits)GlobalFree(m_pBits);
	if(m_hPalette) DeleteObject(m_hPalette);
	if(m_hBmp)	DeleteObject(m_hBmp);
}

LPBITMAPINFO CImage::GetBitMapInfo()
{
	return m_pBmpInfo;
}

BOOL CImage::Save(LPCSTR strFileName)
{
	return SaveFrom(strFileName,m_pBmpInfo,m_pBits);
}
BOOL CImage::SaveFrom(LPCSTR strFileName,LPBITMAPINFO pbi,LPBYTE pBits)
{
	long offbits,fsize;
	int ncolors;
	CFile file;
	BITMAPFILEHEADER bfh;
	WORD header=DIB_HEADER_MARKER;
	if(pBits==NULL || pbi==NULL) return FALSE;
	try
	{
		ncolors=GetNumColors();
		offbits=sizeof(BITMAPFILEHEADER)+sizeof(BITMAPINFOHEADER)+
			ncolors*sizeof(RGBQUAD);
		fsize=pbi->bmiHeader.biHeight*GetBytesRow(&pbi->bmiHeader)+offbits;
		bfh.bfType=DIB_HEADER_MARKER;
		bfh.bfSize=fsize;//文件大小		
		bfh.bfReserved1=0;
		bfh.bfReserved2=0;
		bfh.bfOffBits=offbits;//从文件开始到位流的偏移量
		file.Open(strFileName,CFile::modeWrite|CFile::modeCreate);
		file.Write(&bfh,sizeof(BITMAPFILEHEADER));
		file.Write(&pbi->bmiHeader,sizeof(BITMAPINFOHEADER));
		if(ncolors)
			file.Write(pbi->bmiColors,ncolors*sizeof(RGBQUAD));
		file.Write(pBits,pbi->bmiHeader.biHeight*GetBytesRow(&pbi->bmiHeader));
	}
	catch(...)//CFileException* e)
	{
		file.Close();
		return FALSE;
	}
	return TRUE;
}
/************************************************************************* 
 * 
 * BOOL CImage::Load(LPCSTR strFileName)
 * 
 * 参数: 
 * 
 * LPCSTR strFileName    - 文件名 
 * 
 * 返回值: 
 * 
 * TRUE             - 打开文件成功 
 * FALSE			- 打开文件失败
 *
 * 说明: 
 * 
 * 该函数读取BMP类型文件，不支持以下格式
 * Jpeg，压缩格式，不支持16位，32位。
 * 支持：单色，16色，256色，24位真彩色，以及OS2格式
 * 
 * 
 ************************************************************************/ 
BOOL CImage::Load(LPCSTR strFileName)
{
	BITMAPINFOHEADER bih;
	DWORD dwSize;
	BITMAPFILEHEADER bmfHdr;
	CFile file;
	UINT n,i;
	BOOL	IsOS2=FALSE;
	try
	{
		file.Open(strFileName,CFile::modeRead|CFile::shareDenyNone);
		// 读DIB文件头
		file.Read(&bmfHdr, sizeof(BITMAPFILEHEADER));
		// 是BMP文件吗?
		if (bmfHdr.bfType != DIB_HEADER_MARKER/*"BM"*/)
		{
			file.Close();
			return FALSE;
		}
		if(file.Read(&bih,sizeof(BITMAPINFOHEADER))<sizeof(BITMAPINFOHEADER))
		{
			file.Close();
			return FALSE;	
		}
		else
		{
			if(bih.biSize==sizeof(BITMAPCOREHEADER))//如果是OS2文件
			{
				BITMAPCOREHEADER bch;
				file.Seek(-sizeof(BITMAPINFOHEADER),CFile::current);
				file.Read(&bch,sizeof(BITMAPCOREHEADER));					
				bih.biBitCount=bch.bcBitCount;
				bih.biClrImportant=0;
				if(bch.bcBitCount==24)
					bih.biClrUsed=0;
				else
					bih.biClrUsed=1<<bch.bcBitCount;
				bih.biCompression=BI_RGB;
				bih.biHeight=bch.bcHeight;
				bih.biPlanes=bch.bcPlanes;
				bih.biSize=sizeof(BITMAPINFOHEADER);
				bih.biSizeImage=0;
				bih.biWidth=bch.bcWidth;
				bih.biXPelsPerMeter=0;
				bih.biYPelsPerMeter=0;
				IsOS2=TRUE;
			}
		}
		if(	bih.biBitCount==0	||		//不支持Jpeg
			bih.biBitCount==16	||		//不支持16位
			bih.biBitCount>=32	||		//不支持32位,及以上
			bih.biCompression!=BI_RGB||	//不支持压缩格式
			bih.biPlanes!=1				//不支持多位面结构
			)
		{
			file.Close();
			return FALSE;	
		}
		n=::GetNumColors(&bih);
		if(n>0)
			dwSize =sizeof(BITMAPINFO)+n*sizeof(RGBQUAD);
		else
			dwSize=sizeof(BITMAPINFO);
		if(m_pBmpInfo) GlobalFree(m_pBmpInfo);
		m_pBmpInfo=(BITMAPINFO*)GlobalAllocPtr(GHND,dwSize);
		if(m_pBmpInfo==NULL )
		{
				file.Close();
				return FALSE;
		}
		if(n>0)//如果有颜色表
		{
			if(IsOS2==FALSE)
			{
				//读文件中的颜色表，文件中的颜色表保存方式的字节顺序为：
				//Blue,Green,Red,Reserved ...
				//正好与m_pBmpInfo->bmiColors一致
				if(file.Read(m_pBmpInfo->bmiColors,n*sizeof(RGBQUAD))<n*sizeof(RGBQUAD))
				{
					file.Close();
					GlobalFree(m_pBmpInfo);
					return FALSE;
				}	
			}
			else
			{
				RGBTRIPLE far *rt;
				rt=(RGBTRIPLE far*)GlobalAllocPtr(GHND,n*sizeof(RGBTRIPLE));
				//读文件中的颜色表，文件中的颜色表保存方式的字节顺序为：
				//Blue,Green,Red ...			
				if(rt==NULL || file.Read(rt,n*sizeof(RGBTRIPLE))<n*sizeof(RGBTRIPLE))
				{
					file.Close();
					if(rt) GlobalFree(rt);
					return FALSE;
				}	
				else
				{
					for(i=0;i<n;i++)
					{
						m_pBmpInfo->bmiColors[i].rgbBlue=rt[i].rgbtBlue;
						m_pBmpInfo->bmiColors[i].rgbGreen=rt[i].rgbtGreen;
						m_pBmpInfo->bmiColors[i].rgbRed=rt[i].rgbtRed;
						m_pBmpInfo->bmiColors[i].rgbReserved=0;
					}
					GlobalFree(rt);
				}
			}
		}
		m_pBmpInfo->bmiHeader=bih;
		if(m_pBits) GlobalFree(m_pBits);
		dwSize=GetBytesRow(&bih)*bih.biHeight;
		m_pBits=(LPBYTE)GlobalAllocPtr(GHND,dwSize);
		if(m_pBits==NULL || file.Read(m_pBits,dwSize)!=dwSize)
		{
			if(m_pBmpInfo)GlobalFree(m_pBmpInfo);
			if(m_pBits)GlobalFree(m_pBits);
			file.Close();
			return FALSE;
		}
	}
	catch(...)//CFileException* e)
	{
		if(m_pBmpInfo)GlobalFree(m_pBmpInfo);
		if(m_pBits)GlobalFree(m_pBits);
		file.Close();
		return FALSE;
	}
	if(UpdateInternal()==FALSE)
	{
		return FALSE;
	}
	return TRUE;
}

int GetNumColors(BITMAPINFOHEADER *pbih)
{
	int n=pbih->biClrUsed;
	switch(pbih->biBitCount)
	{
	case 1: //单色
		n=2;
		break;
	case 4:	//16色
		if(n==0) n=16;
		break;
	case 8:
		if(n==0) n=256;
		break;
	default:
		n=0;
	}
	return n;
}

UINT GetBytesRow(BITMAPINFOHEADER *pbih)
{
	return BYTES4(pbih->biWidth*pbih->biBitCount);
}

BOOL CImage::UpdateInternal()
{
	if(m_hBmp)
	{
		DeleteObject(m_hBmp);
		m_hBmp=NULL;
	}
	if(m_hPalette)
	{
		DeleteObject(m_hPalette);
		m_hPalette=NULL;
	}
	m_hPalette = CreateDIBPalette(m_pBmpInfo);
	m_hBmp = DIBToDIBSection(m_pBmpInfo,m_pBits);	
	if (m_hBmp==NULL)
	{
		if(m_hPalette==NULL) DeleteObject(m_hPalette);
		if(m_hBmp==NULL) DeleteObject(m_hBmp);
		return FALSE;
	}
	return TRUE;
}

HPALETTE CImage::CreateDIBPalette(LPBITMAPINFO pbi)
{
    LPLOGPALETTE        lpPal;          // pointer to a logical palette 
    HPALETTE            hPal = NULL;    // handle to a palette 
    int                 i, wNumColors;  // loop index, number of colors in color table 
    LPBITMAPINFO        lpbmi;          // pointer to BITMAPINFO structure (Win3.0) 
    LPBITMAPCOREINFO    lpbmc;          // pointer to BITMAPCOREINFO structure (OS/2) 
    BOOL                bWinStyleDIB;   // Win3.0 DIB? 
 
	if(pbi==NULL) return NULL;
    // get pointer to BITMAPINFO (Win 3.0) 
    lpbmi = pbi; 
 
    // get pointer to BITMAPCOREINFO (OS/2 1.x) 
    lpbmc = (LPBITMAPCOREINFO)pbi; 
 
    // get the number of colors in the DIB  
    wNumColors = ::GetNumColors((BITMAPINFOHEADER*)pbi); 
 
    // is this a Win 3.0 DIB? 
 
    bWinStyleDIB = IS_WIN30_DIB(pbi); 
    if (wNumColors) 
    { 
        // allocate memory block for logical palette 
 
        lpPal = (LPLOGPALETTE)GlobalAllocPtr(GHND, sizeof(LOGPALETTE) + 
                sizeof(PALETTEENTRY) * wNumColors);
 
        // set version and number of palette entries 
 
        lpPal->palVersion = 0x0300; 
        lpPal->palNumEntries = wNumColors; 
 
        // store RGB triples (if Win 3.0 DIB) or RGB quads (if OS/2 DIB) 
        // into palette 
         
        for (i = 0; i < wNumColors; i++) 
        { 
            if (bWinStyleDIB) 
            { 
                lpPal->palPalEntry[i].peRed = lpbmi->bmiColors[i].rgbRed; 
                lpPal->palPalEntry[i].peGreen = lpbmi->bmiColors[i].rgbGreen; 
                lpPal->palPalEntry[i].peBlue = lpbmi->bmiColors[i].rgbBlue; 
                lpPal->palPalEntry[i].peFlags = 0; 
            } 
            else 
            { 
                lpPal->palPalEntry[i].peRed = lpbmc->bmciColors[i].rgbtRed; 
                lpPal->palPalEntry[i].peGreen = lpbmc->bmciColors[i].rgbtGreen; 
                lpPal->palPalEntry[i].peBlue = lpbmc->bmciColors[i].rgbtBlue; 
                lpPal->palPalEntry[i].peFlags = 0; 
            } 
        } 
        // create the palette and get handle to it 
 
        hPal = CreatePalette(lpPal); 
 
        // if error getting handle to palette, clean up and return NULL 
        if (!hPal) 
		{
			GlobalFree(lpPal);
			return NULL; 
		}
    } 
    // return handle to DIB's palette 
    return hPal; 
}

HBITMAP CImage::DIBToDIBSection(LPBITMAPINFO pbi, LPBYTE pBits)
{
    LPBYTE       lpSourceBits; 
    HDC			 hDC = NULL, hSourceDC; 
    HBITMAP      hSourceBitmap, hOldSourceBitmap; 
    DWORD        dwSourceBitsSize; 

	LPBITMAPINFO lpSrcDIB = pbi;
	if (! lpSrcDIB)
		return NULL;

    // Gonna use DIBSections and BitBlt() to do the conversion, so make 'em 
	hDC = GetDC( NULL ); 
    hSourceBitmap = CreateDIBSection( hDC, lpSrcDIB, DIB_RGB_COLORS, (VOID **)&lpSourceBits, NULL, 0 ); 
    hSourceDC = CreateCompatibleDC( hDC ); 
 
    // Flip the bits on the source DIBSection to match the source DIB 
    dwSourceBitsSize = lpSrcDIB->bmiHeader.biHeight * GetBytesRow((LPBITMAPINFOHEADER)pbi); 
    memcpy( lpSourceBits, pBits, dwSourceBitsSize ); 
 
    // Select DIBSections into DCs 
    hOldSourceBitmap = (HBITMAP)SelectObject( hSourceDC, hSourceBitmap ); 
 
    // Set the color tables for the DIBSections 
    if( lpSrcDIB->bmiHeader.biBitCount <= 8 ) 
        SetDIBColorTable( hSourceDC, 0, 1 << lpSrcDIB->bmiHeader.biBitCount, lpSrcDIB->bmiColors ); 

    // Clean up and delete the DCs 
    SelectObject( hSourceDC, hOldSourceBitmap ); 
    DeleteDC( hSourceDC ); 
    ReleaseDC( NULL, hDC ); 
 
    // Flush the GDI batch, so we can play with the bits 
    GdiFlush(); 
 
    return hSourceBitmap;
}
//绘制位图
void CImage::Draw(HDC hdc, int x, int y)
{
	if(m_pBmpInfo==NULL) return;
	/*
	//在256色分辩率下显示真彩色图像
	if (IsRequireDither(hdc))
	{
		int nWidth = m_pBmpInfo->bmiHeader.biWidth;
		int nHeight =m_pBmpInfo->bmiHeader.biHeight;
		CRect rcDest(x, y, x+nWidth, y+nHeight);
		CRect rcSrc(0, 0, nWidth, nHeight);
		return DitherDisplay(pDC, rcDest, rcSrc, dwRop);
	}
	*/
	BITMAP bm; 
	GetObject(m_hBmp, sizeof(bm), &bm); 
	HDC hMemDC;
	HPALETTE hOldPal1,hOldPal2;
	HBITMAP hOldBmp;
	if(m_hBmp==NULL) return;
	hMemDC=CreateCompatibleDC(hdc);
	if(m_hPalette)
		hOldPal1=(HPALETTE)SelectPalette(hMemDC,m_hPalette,FALSE);
	hOldBmp=(HBITMAP)SelectObject(hMemDC,m_hBmp);
	if(m_hPalette)
	{
		hOldPal2=(HPALETTE)SelectPalette(hdc,m_hPalette,TRUE);
		RealizePalette(hdc);
	}
	BitBlt(hdc,x,y,bm.bmWidth,bm.bmHeight,hMemDC,0,0,SRCCOPY);
	SelectObject(hMemDC,hOldBmp);
	if(m_hPalette)
	{
		SelectPalette(hMemDC,hOldPal1,FALSE);
		SelectPalette(hdc,hOldPal2,FALSE);
	}
	DeleteDC(hMemDC);
}

BOOL CImage::IsRequireDither(HDC hdc)
{
	if(m_pBmpInfo==NULL) return FALSE;
		// if device can not display DIB color, dithering DIB
	if (m_pBmpInfo->bmiHeader.biBitCount > 8 && 
		GetDeviceCaps(hdc,BITSPIXEL) <= 8)
		return TRUE;

	return FALSE;
}

//设置P点颜色，
//注意color不是真正的RGB，而是位流中的值
void CImage::SetColor(LPBYTE &p,BYTE &BitsOffset,long Color)
{
	long temp;
	switch(m_pBmpInfo->bmiHeader.biBitCount)
	{
	case 1:
		temp=0x80L>>BitsOffset;
		if(Color)
			*p|=(BYTE)temp;
		else
			*p&=(BYTE)~temp;
		break;
	case 4:
		temp=0xF0>>BitsOffset;
		if(BitsOffset==4)
		{
			*p&=0xF0;
			*p|=(0x0F&(BYTE)Color);
		}
		else
		{
			*p&=0x0F;
			*p|=(BYTE)Color<<4;
		}
		break;
	case 8:
		*p=(BYTE)Color;
		break;
	case 24:
		memcpy(p,&Color,3);		
		break;
	}
}
bool IsColorSame(long i,long j,int Threshold)
{
	long r,g,b;
	r=(i & 0xFFL) - (j & 0xFFL); r=r*r;
	g=((i>>8) & 0xFFL) - ((j>>8) & 0xFFL); g=g*g;
	b=((i>>16) & 0xFFL) - ((j>>16) & 0xFFL); b=b*b;
	if (r+g+b<=Threshold)
		return true;
	return false;
}
//取得点p处的颜色值
//注意 返回值不是真正的RGB，而是位流中的值
long CImage::GetColor(LPBYTE &p,BYTE &BitsOffset)
{
	long ret=0L;
	switch(m_pBmpInfo->bmiHeader.biBitCount)
	{
	case 1:
		memcpy(&ret,p,1);		
		ret=(ret & (0x80L>>BitsOffset))?1:0 ;
		break;
	case 4:
		memcpy(&ret,p,1);
		if(BitsOffset==4)
			ret=0x0FL&ret;
		else
			ret=(0xF0L & ret)>>4;
		break;
	case 8:
		memcpy(&ret,p,1);
		//ret&=0xFFL;
		break;
	case 24:
		memcpy(&ret,p,3);
		//ret&=0xFFFFFFL;
		break;
	}
	return ret;
}

void CImage::RightPixel(LPBYTE &p,BYTE &BitsOffset)
{
	switch(m_pBmpInfo->bmiHeader.biBitCount)
	{
	case 1:
		if(BitsOffset==7)
		{
			BitsOffset=0;
			p++;
		}
		else
			BitsOffset++;
		break;
	case 4:
		if(BitsOffset==4)
		{
			BitsOffset=0;
			p++;
		}
		else
			BitsOffset=4;
		break;
	case 8:
		p++;
		break;
	case 24:
		p+=3;
		break;
	}
}

void CImage::LeftPixel(LPBYTE &p,BYTE &BitsOffset)
{
	switch(m_pBmpInfo->bmiHeader.biBitCount)
	{
	case 1:
		if(BitsOffset==0)
		{
			BitsOffset=7;
			p--;
		}
		else
			BitsOffset--;
		break;
	case 4:
		if(BitsOffset==0)
		{
			BitsOffset=4;
			p--;
		}
		else
			BitsOffset=0;
		break;
	case 8:
		p--;
		break;
	case 24:
		p-=3;
		break;
	}
}

void CImage::UpPixel(LPBYTE &p,int BytesRow)
{
	p+=BytesRow;
}

void CImage::DownPixel(LPBYTE &p,int BytesRow)
{
	p-=BytesRow;
}

void CImage::PushPixel(POSITION_STACK &ps,BITOFFSET_STACK &bs,LPBYTE &p, BYTE &BitsOffset)
{
	ps.push(p);
	if(m_pBmpInfo->bmiHeader.biBitCount==1 ||m_pBmpInfo->bmiHeader.biBitCount==4 )
		bs.push(BitsOffset);
}

void CImage::PopPixel(POSITION_STACK &ps,BITOFFSET_STACK &bs,LPBYTE &p, BYTE &BitsOffset)
{
	p=ps.top();
	ps.pop();
	if(m_pBmpInfo->bmiHeader.biBitCount==1 ||m_pBmpInfo->bmiHeader.biBitCount==4 )
	{
		BitsOffset=bs.top();
		bs.pop();
	}
}

//从点pnt开始填充位图
//pnt的坐标从位图的左上角开始计算，X向右，Y向下
BOOL CImage::Fill(POINT pnt, COLORREF color)
{
	long OldColor;
	POSITION_STACK ps;
	BITOFFSET_STACK bs;
	BOOL ret;
	LPBYTE p;
	BYTE	BitOffset;//从0开始,从高位到低位
	CSize sz;
	sz.cx=GetWidth();
	sz.cy=GetHeight();		
	if((pnt.x>=sz.cx) || (pnt.y>=sz.cy))
	{
		return FALSE;
	}
	if(m_pBmpInfo->bmiHeader.biBitCount==0)//不支持JPEG
		return FALSE;
	else
	{
		if(m_pBmpInfo->bmiHeader.biBitCount==24)
		{
			//交换颜色的RGB值为BGR
			OldColor=color;
			color=0x00FF00&OldColor;
			color+=OldColor<<16;
			color+=OldColor>>16;
			color&=0xFFFFFF;
		}
		else
		{
			color=GetNearColorNum(color)	;
		}
	}
	long RowOff;//填充点在该行的偏移
	RowOff=(sz.cy-pnt.y-1)*GetBytesRow((LPBITMAPINFOHEADER)m_pBmpInfo);
	p=m_pBits;
	switch(m_pBmpInfo->bmiHeader.biBitCount)
	{
	case 1://单色
		p+=RowOff+pnt.x/8;
		BitOffset=pnt.x%8;
		break;
	case 4://16色
		p+=RowOff+pnt.x/2;
		BitOffset=(pnt.x%2)?4:0 ;
		break;
	case 8:
		p+=RowOff+pnt.x;
		BitOffset=0;
		break;
	case 24:
		p+=RowOff+pnt.x*3;
		BitOffset=0;
		break;
	default :
		ret=FALSE;
	}
	OldColor=GetColor(p,BitOffset);
	if(OldColor==color)
		return TRUE;
	long ByteLine=GetBytesRow((LPBITMAPINFOHEADER)m_pBmpInfo);
	LPBYTE pTemp;
	BYTE bTemp;
	LPBYTE pl,pr,pl2,pr2;
	BYTE   bl,br,bl2,br2;	
	BOOL	Found=FALSE;
	int		BytesPixel;
	BytesPixel=m_pBmpInfo->bmiHeader.biBitCount>>3; //除以8
	POINT CurPnt;
	//第一个象素入栈
	PushPixel(ps,bs,p,BitOffset);
	while((!ps.empty()))
	{
		PopPixel(ps,bs,pTemp,bTemp);
		SetColor(pTemp,bTemp,color);
		pl=pr=pTemp;
		bl=br=bTemp;
		CurPnt.y=sz.cy-1-(pTemp-m_pBits)/ByteLine;
		switch(m_pBmpInfo->bmiHeader.biBitCount)
		{
		case 1:
			CurPnt.x=(pTemp-m_pBits)%ByteLine*8+bTemp;
			break;
		case 4:
			CurPnt.x=(pTemp-m_pBits)%ByteLine*2+(bTemp==0?0:1);
			break;
		case 8:
			CurPnt.x=(pTemp-m_pBits)%ByteLine;
			break;
		case 24:
			CurPnt.x=(pTemp-m_pBits)%ByteLine/3;
			break;
		}
		while(CurPnt.x)//如果pl不是最左象素
		{
			CurPnt.x--;
			LeftPixel(pl,bl);
			//if(GetColor(pl,bl)==OldColor)//如果需要填充
			if(IsColorSame(GetColor(pl,bl),OldColor,1000))
				SetColor(pl,bl,color);
			else
			{
				RightPixel(pl,bl);
				break;
			}
		}
		while((CurPnt.x<sz.cx-1))//如果pr不是最右象素
		{
			CurPnt.x++;
			RightPixel(pr,br);
			//if(GetColor(pr,br)==OldColor)//如果需要填充
			if(IsColorSame(GetColor(pr,br),OldColor,1000))
				SetColor(pr,br,color);
			else
			{
				LeftPixel(pr,br);
				break;
			}
		}
		pl2=pl;pr2=pr;
		bl2=bl;br2=br;
		if(CurPnt.y) 
		{	
			UpPixel(pl2,ByteLine);//上一行
			UpPixel(pr2,ByteLine);
			Found=FALSE;
			while(1)
			{
				if(Found==FALSE)
				{
					//if(GetColor(pl2,bl2)==OldColor)//如果等于需要填充色
					if(IsColorSame(GetColor(pl2,bl2),OldColor,1000))
					{
						PushPixel(ps,bs,pl2,bl2);
						Found=TRUE;
					}
				}
				else
				{
					//if(GetColor(pl2,bl2)!=OldColor)//如果不等于需要填充色
					if(IsColorSame(GetColor(pl2,bl2),OldColor,1000)==false)
						Found=FALSE;
				}
				if(pl2==pr2 && bl2==br2)
					break;				
				RightPixel(pl2,bl2);					
			}
		}	
		pl2=pl;pr2=pr;
		bl2=bl;br2=br;
		Found=FALSE;
		if(CurPnt.y<sz.cy-1)
		{
			DownPixel(pl2,ByteLine); //下一行
			DownPixel(pr2,ByteLine);
			Found=FALSE;
			while(1)
			{
				if(Found==FALSE)
				{
					//if(GetColor(pl2,bl2)==OldColor)//如果等于需要填充色
					if(IsColorSame(GetColor(pl2,bl2),OldColor,1000))
					{
						PushPixel(ps,bs,pl2,bl2);
						Found=TRUE;
					}
				}
				else
				{
					//if(GetColor(pl2,bl2)!=OldColor)//如果等于需要填充色
					if(IsColorSame(GetColor(pl2,bl2),OldColor,1000)==false)
						Found=FALSE;
				}
				if(pl2==pr2 && bl2==br2)
					break;				
				RightPixel(pl2,bl2);
			}
		}
	}
	ret=UpdateInternal();
	return ret;
}
int CImage::GetWidth()
{
	if(m_pBmpInfo==NULL) return 0;
	return m_pBmpInfo->bmiHeader.biWidth;
}

int CImage::GetHeight()
{
	if(m_pBmpInfo==NULL) return 0;
	return m_pBmpInfo->bmiHeader.biHeight;
}

//在颜色表中找出与color最匹配的颜色
//返回，颜色表的颜色号
int CImage::GetNearColorNum(COLORREF color)
{
	int r,g,b,off;
	r=0xFF&color;
	g=0xFF&(color>>8);
	b=0xFF&(color>>16);
	if(m_pBmpInfo->bmiHeader.biBitCount>8) return 0;
	int i,num,numcolor=GetNumColors();
	//首先找到匹配的红色
	off=abs(r-m_pBmpInfo->bmiColors[0].rgbRed);
	num=0;
	for(i=1;i<numcolor;i++)
	{
		if(off==0)
		{
			break;
		}
		else
		{
			if(abs(r-m_pBmpInfo->bmiColors[i].rgbRed)<off)
			{
				num=i;
				off=abs(r-m_pBmpInfo->bmiColors[i].rgbRed);
			}
		}
	}
	r=m_pBmpInfo->bmiColors[num].rgbRed;
	if(num+1>=numcolor) return num;
	//找到匹配的蓝色	
	off=abs(g-m_pBmpInfo->bmiColors[num].rgbGreen);
	for(i=num+1;i<numcolor;i++)
	{
		if(m_pBmpInfo->bmiColors[i].rgbRed!=r)
			continue;
		if(off==0)
		{
			break;
		}
		else
		{
			if(abs(g-m_pBmpInfo->bmiColors[i].rgbGreen)<off)
			{
				num=i;
				off=abs(g-m_pBmpInfo->bmiColors[i].rgbGreen);
			}
		}
	}
	g=m_pBmpInfo->bmiColors[num].rgbGreen;
	if(num+1>=numcolor) return num;
	//找到匹配的绿色
	off=abs(b-m_pBmpInfo->bmiColors[num].rgbBlue);
	for(i=num+1;i<numcolor;i++)
	{
		if((m_pBmpInfo->bmiColors[i].rgbRed!=r) 
			&& (m_pBmpInfo->bmiColors[i].rgbGreen!=g))
			continue;
		if(off==0)
		{
			break;
		}
		else
		{
			if(abs(b-m_pBmpInfo->bmiColors[i].rgbBlue)<off)
			{
				num=i;
				off=abs(b-m_pBmpInfo->bmiColors[i].rgbBlue);
			}
		}
	}
	return num;
}

void CImage::SetColorTable(int index, COLORREF color)
{
	int ncolors=GetNumColors();
	if(index>=0 && index<ncolors)
	{
		m_pBmpInfo->bmiColors[index].rgbRed=(BYTE)(0xFFL&color);
		m_pBmpInfo->bmiColors[index].rgbGreen=(BYTE)(0xFFL&(color>>8));
		m_pBmpInfo->bmiColors[index].rgbBlue=(BYTE)(0xFFL&(color>>16));
		UpdateInternal();
	}
}

COLORREF CImage::GetColorTable(int index)
{
	int ncolors=GetNumColors();
	COLORREF c=0;
	if(index>=0 && index<ncolors)
	{
		c=RGB(	m_pBmpInfo->bmiColors[index].rgbRed,
			m_pBmpInfo->bmiColors[index].rgbGreen,
			m_pBmpInfo->bmiColors[index].rgbBlue);
	}
	return c;
}
